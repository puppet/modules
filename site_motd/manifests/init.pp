# Class: site_motd
#
# Provision login banner and related files
#
class site_motd (
  String $global        = '',
  String $globalwarning = '',
  String $bannername    = 'lydia-rainbow',
) {
  include ::profile::base::insomnia247_dir

  file {
    default:
      ensure => file,
      mode   => '0755',
      owner  => 'root',
      group  => 'root';
    '/etc/motd':
      content => '';
    '/etc/news.d':
      ensure => directory;
    '/opt/insomnia247/motd':
      ensure => directory;

    # Banner
    '/opt/insomnia247/motd/banner':
      source => "puppet:///modules/site_motd/banners/${bannername}.bin";

    # Statistics
    '/opt/insomnia247/motd/cronjob.rb':
      mode   => '0750',
      source => 'puppet:///modules/site_motd/scripts/cronjob.rb';
    '/opt/insomnia247/motd/showstat.rb':
      source => 'puppet:///modules/site_motd/scripts/showstat.rb';

    # News
    '/opt/insomnia247/motd/news.rb':
      source => 'puppet:///modules/site_motd/scripts/news.rb';

    # Profile scripts
    '/etc/profile.d/10-bannerprint.sh':
      content => 'cat /opt/insomnia247/motd/banner';
    '/etc/profile.d/12-showstats.sh':
      content => 'ruby /opt/insomnia247/motd/showstat.rb';
    '/etc/profile.d/14-news.sh':
      content => 'ruby /opt/insomnia247/motd/news.rb';
  }

  cron { 'motd_update_stats':
    command => 'ruby /opt/insomnia247/motd/cronjob.rb',
    user    => 'root',
    minute  => '*/5',
  }

  file { '/etc/news.d/global':
    ensure  => empty($global) ? { true => absent, default => file },
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => $global,
  }

  file { '/etc/news.d/globalwarning':
    ensure  => empty($globalwarning) ? { true => absent, default => file },
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => $globalwarning,
  }
}
