#!/usr/bin/env ruby
# frozen_string_literal: true

exit unless $stdout.tty?

require 'cli/ui'
CLI::UI::StdoutRouter.enable

# Global news
if File.exist? '/etc/news.d/globalwarning'
  CLI::UI::Frame.open('{{bold:Important!}}', color: :red) do
    puts File.read '/etc/news.d/globalwarning'
  end
elsif File.exist? '/etc/news.d/global'
  CLI::UI::Frame.open('{{bold:News}}', color: :cyan) do
    puts File.read '/etc/news.d/global'
  end
end

# User specific news
if File.exist? "/etc/news.d/#{ENV['USER']}"
  CLI::UI::Frame.open("{{bold:Message for #{ENV['USER']}}}", color: :yellow) do
    puts File.read "/etc/news.d/#{ENV['USER']}"
  end
end
