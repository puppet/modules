#!/usr/bin/env ruby
# frozen_string_literal: true

require 'yaml'

# Build skeleton data hash
stats = {
  'cpu' => {
    'count' => 24,
    'type'  => 'AMD Ryzen 9 3900X @ 3.8GHz',
    'load'  => nil
  },
  'memory' => {
    'total' => nil,
    'free'  => 0
  },
  'disk' => {
    'root' => {
      'total' => nil,
      'free'  => nil
    },
    'coldstorage' => {
      'total' => nil,
      'free'  => nil
    }
  },
  'users' => {
    'total'  => nil,
    'active' => nil,
    'online' => nil
  }
}

# Read CPU load
stats['cpu']['load'] = ((File.read('/proc/loadavg').split[0].to_f / stats['cpu']['count'].to_f) * 100).round(2)

# Read memory stats
File.readlines('/proc/meminfo').each do |line|
  if line.start_with? 'MemTotal'
    stats['memory']['total'] = line.split[1].to_i / 1024
  elsif line.start_with? 'MemAvailable'
    stats['memory']['free'] = line.split[1].to_i / 1024
  end
end

# Gather disk stats
root = %x(df -h /).split("\n").last.split
cold = %x(df -h /coldstorage).split("\n").last
cold = cold.split unless cold.nil?

stats['disk']['root']['total'] = root[1]
stats['disk']['root']['free']  = root[3]
unless cold.nil?
  stats['disk']['coldstorage']['total'] = cold[1]
  stats['disk']['coldstorage']['free']  = cold[3]
end

# Gather user stats
stats['users']['total']  = %x(ls /home).split("\n").size
stats['users']['online'] = %x(w).split("\n")[2..-1].map{ |l| l.split[0] }.sort.uniq.size
stats['users']['active'] = %x(last -w).split("\n")[0..-3].map{ |l| l.split[0] }.sort.uniq.size

# Write out stats file
File.write("#{File.path(__dir__)}/stats.yaml", YAML.dump(stats))
