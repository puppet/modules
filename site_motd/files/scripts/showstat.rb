#!/usr/bin/env ruby
# frozen_string_literal: true

exit unless $stdout.tty?

require 'yaml'
require 'cli/ui'

CLI::UI::StdoutRouter.enable

stats = YAML.load_file "#{File.path(__dir__)}/stats.yaml"

CLI::UI::Frame.open('{{bold:CPU}}', color: :green) do
  puts CLI::UI::fmt "{{cyan:Type}} #{stats['cpu']['count']} x #{stats['cpu']['type']}"
  puts CLI::UI::fmt "{{cyan:Load}} #{stats['cpu']['load']} %"
  CLI::UI::Frame.divider('{{bold:Memory}}', color: :green)
  puts CLI::UI::fmt "{{cyan:Free}}  #{stats['memory']['free']}M"
  puts CLI::UI::fmt "{{cyan:Total}} #{stats['memory']['total']}M"
  CLI::UI::Frame.divider('{{bold:Disk (Free / Total)}}', color: :green)
  puts CLI::UI::fmt "{{cyan:SSD array}}   #{stats['disk']['root']['free']} / #{stats['disk']['root']['total']}"
  puts CLI::UI::fmt "{{cyan:Coldstorage}} #{stats['disk']['coldstorage']['free']} / #{stats['disk']['coldstorage']['total']}"
  CLI::UI::Frame.divider('{{bold:Users}}', color: :green)
  puts CLI::UI::fmt "{{cyan:Total}}             #{stats['users']['total']}"
  puts CLI::UI::fmt "{{cyan:Active this month}} #{stats['users']['active']}"
  puts CLI::UI::fmt "{{cyan:Online now}}        #{stats['users']['online']}"
end
