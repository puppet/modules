# Class: site_rundeck::client
#
# Class to manage machines which rundeck can access.
#
class site_rundeck::client (
  Boolean $ldap_user = true,
  String  $homedir   = '/home/rundeck',
  String  $publickey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDMfeY+7kBZdVRcLZ\
gfFFmiAPzyYT5FmTWKf5rtC4b1m80Ou/c8RjB6gM4ACA8HQ1nxYUskvYEu3jPA0Wwh/qn5fCBJvXrP\
giW+Ftxew1iMnN7lkH9XSAAujgdSPun0a46Y5PGJCbnEgLzIU9Gz/aom6TYDKxO8aXmghZwYb8fSrn\
CjHEirED/rudoHD0sHd5NnG/soF1PA4UZGLmxSnd5JxkHQWLq+ssd/3308dD+AVRIq/jrPvbpMEojF\
EcKPtVE1ZpDPVKoL2Tm+roJCQ0XReo7LgMAikXV+z9oyx/Lh3SC9xBifkfzgxdZHtRIAjSqir6S0Wf\
CujtvTfvHsL7IP\n",
  Array   $groups    = ['sudo'],
) {
  unless $ldap_user {
    group { 'rundeck':
      gid => 502,
    }

    -> user { 'rundeck':
      ensure  => present,
      uid     => 502,
      groups  => $groups,
      comment => 'Rundeck user',
      home    => $homedir,
      shell   => '/bin/bash',
    }

    User['rundeck'] -> File[$homedir]
  }

  file {
    default:
      ensure => directory,
      owner  => 'rundeck',
      group  => 'rundeck',
      mode   => '0750';
    $homedir:
      ;
    "${homedir}/.ssh":
      ;
    "${homedir}/.ssh/authorized_keys":
      ensure  => file,
      mode    => '0600',
      content => $publickey;
    '/etc/sudoers.d/rundeck':
      ensure  => file,
      mode    => '0440',
      owner   => 'root',
      group   => 'root',
      content => 'rundeck ALL=(ALL:ALL) NOPASSWD:ALL';
  }
}
