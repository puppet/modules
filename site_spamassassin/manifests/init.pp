# Class: site_spamassassin
#
# Install spamassassin and manage config
#
class site_spamassassin (
  Array $packages = [
    'spamassassin',
    'sa-compile',
  ],
) {
  # Install packages
  package { $packages:
    ensure => installed,
  }

  # Manage config files
  -> file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644';
    '/etc/spamassassin/v341.pre':
      content => 'loadplugin Mail::SpamAssassin::Plugin::PDFInfo';
    '/etc/spamassassin/local.cf':
      content => "required_score 4.5\nscore __RCVD_IN_DNSWL 0";
    '/etc/default/spamd':
      source => 'puppet:///modules/site_spamassassin/etc-default-spamassassin';
  }

  # Manage service
  ~> service { 'spamd':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }
}
