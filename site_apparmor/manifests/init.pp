# Class: site_apparmor
#
class site_apparmor {
  package { ['apparmor', 'apparmor-utils']:
    ensure => installed,
  }

  -> service { 'apparmor':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }

  file { '/etc/apparmor.d/':
    ensure  => directory,
    recurse => true,
    source  => 'puppet:///modules/site_apparmor/profiles',
    notify  => Service['apparmor'],
  }
}
