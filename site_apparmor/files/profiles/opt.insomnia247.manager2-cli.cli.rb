#include <tunables/global>

/opt/insomnia247/manager2-cli/cli.rb {
  #include <abstractions/base>
  #include <abstractions/bash>
  #include <abstractions/consoles>
  #include <abstractions/nameservice>
  #include <abstractions/openssl>
  #include <abstractions/ruby>
  #include <abstractions/ubuntu-browsers.d/multimedia>
  #include <abstractions/user-tmp>

  /etc/apt/apt.conf.d/ r,
  /etc/apt/apt.conf.d/* r,
  /etc/apt/sources.list r,
  /etc/apt/sources.list.d/ r,
  /etc/apt/sources.list.d/*.list r,
  /etc/dpkg/dpkg.cfg r,
  /etc/dpkg/dpkg.cfg.d/ r,
  /etc/shells r,
  /opt/insomnia247/manager2-cli/** r,
  /proc/filesystems r,
  /proc/sys/kernel/ngroups_max r,
  /bin/which mrix,
  /bin/dash mrix,
  /usr/bin/dash mrix,
  /usr/bin/dpkg mrix,
  /usr/bin/google-authenticator mrix,
  /usr/bin/quota mrix,
  /usr/bin/ruby* ix,
  /usr/bin/which rix,
  /usr/share/dpkg/cputable r,
  /usr/share/dpkg/tupletable r,
  /usr/share/rubygems-integration/** r,
  /var/cache/apt/pkgcache.bin r,
  /var/lib/apt/lists/ r,
  /var/lib/apt/lists/* r,
  /var/lib/apt/lists/auxfiles/ rw,
  /var/lib/apt/lists/partial/ w,
  /var/lib/gems/** r,
  owner @{HOME}/.config/insomnia247/api_key r,
  owner @{HOME}/.inputrc r,
  owner /proc/*/fd/ r,
  owner /proc/*/mounts r,
  owner /var/lib/apt/lists/lock w,
}
