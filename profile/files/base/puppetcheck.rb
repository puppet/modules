#!/usr/bin/env ruby
# frozen_string_literal: true

require 'date'
require 'net/http'
require 'socket'
require 'yaml'

reports = []

summary_location = '/opt/puppetlabs/puppet/cache/state/last_run_report.yaml'

exit unless File.exist? summary_location

reports.push 'was over 24 hours ago' if DateTime.parse(File.mtime(summary_location).to_s) < DateTime.now - 1

begin
  content = File.read(summary_location).split("\n")[1..-1].join("\n")
  summary = YAML.safe_load(content)
rescue Psych::SyntaxError
  reports.push 'has left an unparsable summary file'
  exit
end

summary['metrics']['resources']['values'].each do |v|
  if v.first == 'failed'
    reports.push "has #{v.last} failed item(s)" unless v.last.zero?
  elsif v.first == 'failed_to_restart'
    reports.push "has #{v.last} failed to restart item(s)" unless v.last.zero?
  end
end

exit if reports.size.zero?
report = "#{Socket.gethostname}: Puppet run #{reports.join ' and '}."

uri = URI 'https://nanoapi.insomnia247.nl/v1/message'
['Cool_Fire', 'cFire'].each do |dest|
  Net::HTTP.post_form(
    uri,
    'key'     => ENV['APIKEY'],
    'dest'    => dest,
    'message' => report
  )
end
