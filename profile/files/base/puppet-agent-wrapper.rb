#!/usr/bin/env ruby
# frozen_string_literal: true

run = false

summary_location = '/opt/puppetlabs/puppet/cache/state/last_run_report.yaml'

# Run if no report exists
run = true unless File.exist? summary_location

# Run if last run was over 11 hours ago
run = true if Time.now.to_i - File.stat(summary_location).mtime.to_i > 11 * 60 * 60

%x(#{ARGV.join ' '}) if run
