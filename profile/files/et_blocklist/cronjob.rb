#!/usr/bin/env ruby
# frozen_string_literal: true

require 'net/http'

blocklist = Net::HTTP.get URI.parse 'https://rules.emergingthreats.net/fwrules/emerging-Block-IPs.txt'
bl = blocklist.each_line.map { |e| e.strip unless e.start_with? '#', "\n" }.compact

if bl.include? '</html>'
  puts 'Something went wrong when downloading ET blocklist. Got an HTML file instead of the blocklist.'
  puts 'Writing to HTTP response to /root/blocklist.html.txt for investigation.'
  File.write('/root/blocklist.html.txt', blocklist, 'w')
  exit(1)
end

system '/sbin/iptables -F et-blocklist'

bl.each do |e|
  system "/sbin/iptables -A et-blocklist -s #{e} -j DROP"
end
