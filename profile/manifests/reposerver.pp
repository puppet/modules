# Installs and configures nginx
#
# @param path Location on the filesystem where the repo files are stored
# @param vhost nginx vhost domain where the repo files will be served
#
class profile::reposerver(
  String $path  = '/var/www/repo',
  String $vhost = 'repo.insomnia247.nl',
) {
  include nginx

  file { $path:
    ensure  => directory,
    recurse => true,
  }

  -> nginx::resource::server { $vhost:
    www_root => $path,
  }
}
