# Class: profile::manager_api
#
# - Set up OS packages
# - Manage certificates
# - Manage apache vhost
# - Deploy manger2-api git repository
# - Manage API configuration file
#
class profile::manager_api (
  Hash   $apiconf,
  String $sshkey,
  String $sshfingerprint,
  String $vhost = 'api.insomnia247.nl',
) {
  include ::profile::base::packages
  include ::profile::domaincerts
  include ::site_apache
  include ::site_rundeck::client
  include ::systemd

  # Create apache vhost
  site_apache::vhost::system { $vhost:
    customdirectives => "
  PassengerAppRoot /var/www/vhosts/${vhost}/
  RailsEnv         production
  PassengerUser    www-data
  PassengerGroup   www-data"
  }

  # Pull down repository
  vcsrepo { "/var/www/vhosts/${vhost}/":
    ensure   => latest,
    provider => git,
    user     => 'www-data',
    group    => 'www-data',
    source   => 'https://git.insomnia247.nl/shells/manager2-api.git',
  }

  ~> exec { 'bundle_install':
    command     => '/usr/bin/bundle install',
    cwd         => "/var/www/vhosts/${vhost}/",
    user        => 'www-data',
    refreshonly => true,
  }

  # Write configuration file for application
  file { "/var/www/vhosts/${vhost}/config.yaml":
    ensure  => file,
    owner   => 'www-data',
    group   => 'www-data',
    mode    => '0750',
    content => inline_template('<%= @apiconf.to_yaml %>'),
  }

  ~> exec { "${vhost} Touch restart":
    command     => "/usr/bin/touch /var/www/vhosts/${vhost}/tmp/restart.txt",
    user        => 'www-data',
    refreshonly => true,
  }

  # Create logrotate rule for application log
  logrotate::rule { 'production_log':
    ensure       => present,
    rotate       => 12,
    rotate_every => 'month',
    missingok    => true,
    compress     => true,
    ifempty      => false,
    path         => "/var/www/vhosts/${vhost}/logs/production.log",
    postrotate   => "/usr/bin/touch /var/www/vhosts/${vhost}/tmp/restart.txt",
  }

  # Configure ssh key so api can interact with git
  file {
    default:
      ensure => file,
      owner  => 'www-data',
      group  => 'www-data';
    '/var/www/.ssh':
      ensure => directory,
      mode   => '0770';
    '/var/www/.ssh/id_rsa':
      mode    => '0600',
      content => Sensitive($sshkey);
    '/var/www/.ssh/known_hosts':
      mode    => '0644',
      content => $sshfingerprint;
  }

  Class['Profile::Base::Packages']
  -> Site_apache::Vhost::System[$vhost]
  -> Vcsrepo["/var/www/vhosts/${vhost}/"]
  -> File["/var/www/vhosts/${vhost}/config.yaml"]
  -> Logrotate::Rule['production_log']
  -> File['/var/www/.ssh']

  Vcsrepo["/var/www/vhosts/${vhost}/"]
  ~> Exec["${vhost} Touch restart"]
}
