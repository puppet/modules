# Class: profile::openvpn::server
#
#
class profile::openvpn::server (
  String  $publicip,
  String  $publicip6,
  String  $proto    = 'tcp-server',
  Boolean $isserver = true,
  String  $key      = $::profile::openvpn::params::key,
  String  $srvip    = $::profile::openvpn::params::srvip,
  Integer $port     = $::profile::openvpn::params::port,
  String  $keyfile  = $::profile::openvpn::params::keyfile,
  String  $ifrange  = $::profile::openvpn::params::ifrange,
  String  $ifrange6 = $::profile::openvpn::params::ifrange6,
  String  $cipher   = $::profile::openvpn::params::cipher,
  String  $auth     = $::profile::openvpn::params::auth,
  String  $logfile  = $::profile::openvpn::params::logfile,
) inherits profile::openvpn::params {
  # The OpenVPN bit
  file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0640',
      notify => Service['openvpn'];
    '/etc/openvpn/server.conf':
      content => template('profile/openvpn.conf.erb');
    $keyfile:
      content => Sensitive($key);
  }

  service { 'openvpn':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }

  Class['::profile::base::packages']
  -> File['/etc/openvpn/server.conf']

  # The network config bit
  sysctl {
    'net.ipv4.ip_forward':
      value => '1';
    'net.ipv6.conf.all.forwarding':
      value => '1';
    }

  firewall {
    default:
      table => 'nat',
      chain => 'PREROUTING',
      proto => 'all',
      jump  => 'DNAT';
    '100 DNAT to tunnel':
      destination => $publicip,
      todest      => "${ifrange}.10";
    '100 DNAT to tunnel v6':
      destination => $publicip6,
      todest      => "${ifrange6}::10",
      provider    => 'ip6tables';
  }

  firewall {
    default:
      table    => 'nat',
      chain    => 'POSTROUTING',
      proto    => 'all',
      jump     => 'SNAT',
      outiface => '! tun+';
    '100 SNAT to internet':
      source   => "${ifrange}.10",
      tosource => $publicip;
    '100 SNAT to internet v6':
      source   => "${ifrange6}::10",
      tosource => $publicip6,
      provider => 'ip6tables';
  }

  firewall {
    default:
      chain   => 'FORWARD',
      ctstate => ['RELATED', 'ESTABLISHED'],
      proto   => 'all',
      action  => 'accept';
    '110 forward related and established':;
    '110 forward related and established v6':
      provider => 'ip6tables';
  }
}
