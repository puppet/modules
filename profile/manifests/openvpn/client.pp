# Class: profile::openvpn::client
#
#
class profile::openvpn::client (
  Array   $routes   = [],
  Array   $routes6  = [],
  String  $proto    = 'tcp-client',
  Boolean $isserver = false,
  String  $key      = $::profile::openvpn::params::key,
  String  $srvip    = $::profile::openvpn::params::srvip,
  Integer $port     = $::profile::openvpn::params::port,
  String  $keyfile  = $::profile::openvpn::params::keyfile,
  String  $ifrange  = $::profile::openvpn::params::ifrange,
  String  $ifrange6 = $::profile::openvpn::params::ifrange6,
  String  $cipher   = $::profile::openvpn::params::cipher,
  String  $auth     = $::profile::openvpn::params::auth,
  String  $logfile  = $::profile::openvpn::params::logfile,
) inherits profile::openvpn::params {
  file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0640',
      notify => Service['openvpn'];
    '/etc/openvpn/client.conf':
      content => template('profile/openvpn.conf.erb');
    $keyfile:
      content => Sensitive($key);
  }

  service { 'openvpn':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }

  Class['::profile::base::packages']
  -> File['/etc/openvpn/client.conf']
}
