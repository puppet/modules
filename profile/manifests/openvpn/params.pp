# Class: profile::openvpn::params
#
#
class profile::openvpn::params (
  String  $key,
  String  $srvip,
  Integer $port     = 11011,
  String  $keyfile  = '/etc/openvpn/secret.key',
  String  $ifrange  = '192.168.221',
  String  $ifrange6 = 'fd5d:12c9:4221',
  String  $cipher   = 'AES-128-CBC',
  String  $auth     = 'SHA256',
  String  $logfile  = '/var/log/openvpn.log',
) {}
