# Class to create a (bunch of) reverse ssh tunnel(s)
#
class profile::sshtunnel::destination (
  String $privatekey,
  String $tunneluser = 'i2am',
  Hash   $tunnels    = {},
) {
  include systemd

  # Configure the main autossh class
  class{ '::autossh':
    enable            => true,
    user              => $tunneluser,
    ssh_tcpkeepalives => true,
  }

  # Provision ssh private key
  -> file { "/home/${tunneluser}/.ssh/id_rsa":
    ensure  => file,
    owner   => $tunneluser,
    group   => $tunneluser,
    mode    => '0600',
    content => $privatekey,
  }

  # Set up autossh tunnels
  $defaults = {
    enable          => true,
    user            => $tunneluser,
    tunnel_type     => 'reverse',
    port            => '8443',
    hostport        => '22',
    remote_ssh_port => '22',
    remote_ssh_user => $tunneluser,
    bind            => '0.0.0.0',
    monitor_port    => '0',
    keyfile         => "/home/${tunneluser}/.ssh/id_rsa",
  }

  create_resources( '::autossh::tunnel', $tunnels, $defaults)

  # Create systemd dropins to ensure next-hop is set for each route before attempting to connect
  $tunnels.each |String $tunnelname, Hash $options| {
    systemd::dropin_file{ "autossh-${tunnelname}-prestart.conf":
      unit    => "autossh-${tunnelname}.service",
      content => "[Service]\nExecStartPre=/bin/ping -c 2 ${options['remote_ssh_host']}\n"
    }
  }
}
