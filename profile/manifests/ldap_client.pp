# Class to set up ldap client config
#
# @param server LDAP server URI
# @param base base search path for ldap
# @param user username for ldap admin
# @param pass password for ldap admin
# @param packages array of packages to install
# @param nslcd localtion of the nslcd config
# @param nssldap location of nss ldap config
# @param nsssecret location of nss ldap secret
# @param pamsecret location of pam ldap secret
# @param nssfile location of nnswitch config
#
class profile::ldap_client (
  String $server,
  String $base,
  String $user,
  String $pass,
  Array  $packages  = ['ldapscripts', 'ldap-utils', 'libnss-ldapd'],
  String $nslcd     = '/etc/nslcd.conf',
  String $nscd      = '/etc/nscd.conf',
  String $nssldap   = '/etc/libnss-ldap.conf',
  String $nsssecret = '/etc/ldap.secret',
  String $pamsecret = '/etc/pam_ldap.secret',
  String $nsswfile  = '/etc/nsswitch.conf',
) {

  # Install packages
  package { $packages:
    ensure => installed,
    tag    => ['pkgs-ldap'],
  }

  # Write LDAP config
  file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0744',
      tag    => ['files-ldap'];
    $nslcd:
      mode    => '0700',
      content => template('profile/ldap_client/nslcd.conf.erb'),
      notify  => Service['nslcd'];
    $nscd:
      source => 'puppet:///modules/profile/ldap_client/nscd.conf',
      notify => Service['nscd'];
    $nssldap:
      content => template('profile/ldap_client/libnss-ldap.conf.erb'),
      notify  => Service['nslcd'];
    $nsssecret:
      mode    => '0700',
      content => Sensitive($pass);
    $pamsecret:
      mode    => '0700',
      content => Sensitive($pass);
    $nsswfile:
      source => 'puppet:///modules/profile/ldap_client/nsswitch.conf',
      notify => Service['nslcd'];
  }

  # Service definitions
  service {
    default:
      ensure     => running,
      enable     => true,
      hasrestart => true,
      hasstatus  => true,
      tag        => ['services-ldap'];
    'nslcd':
    ;
    'nscd':
    ;
  }

  # Ensure ordering
  Package<| tag == 'pkgs-loose' |>
  -> File<| tag == 'files-ldap' |>
  -> Service<| tag == 'services-ldap' |>
}
