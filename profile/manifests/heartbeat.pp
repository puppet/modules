# Class: profile::heartbeat
#
# - Set up OS packages
# - Manage certificates
# - Manage apache vhost
# - Deploy heartbeat git repository
#
class profile::heartbeat (
  String $vhost = 'heartbeat.insomnia247.nl',
) {
  include ::profile::base::packages
  include ::profile::domaincerts
  include ::site_apache
  include ::site_ssh::keys_only
  include ::site_rundeck::client
  include ::systemd

  # Create apache vhost
  site_apache::vhost::system { $vhost:
    aliases          => [ 'status.insomnia247.nl' ],
    customdirectives => "
  PassengerAppRoot /var/www/vhosts/${vhost}/
  RailsEnv         production
  PassengerUser    www-data
  PassengerGroup   www-data"
  }

  # Pull down repository
  vcsrepo { "/var/www/vhosts/${vhost}/":
    ensure   => latest,
    provider => git,
    user     => 'www-data',
    group    => 'www-data',
    source   => 'https://github.com/cFire/heartbeat.git',
  }

  ~> exec { 'bundle_install':
    command     => '/usr/bin/bundle install --path vendor/bundle',
    cwd         => "/var/www/vhosts/${vhost}/",
    user        => 'www-data',
    refreshonly => true,
  }

  ~> exec { "${vhost} Touch restart":
    command     => "/usr/bin/touch /var/www/vhosts/${vhost}/tmp/restart.txt",
    user        => 'www-data',
    refreshonly => true,
  }

  cron { 'heartbeat_checker':
    command     => "cd /var/www/vhosts/${vhost}/; bundle exec ruby checker.rb",
    environment => [ 'MAILTO=""' ],
    user        => 'www-data',
    minute      => '*/5',
  }

  Class['Profile::Base::Packages']
  -> Site_apache::Vhost::System[$vhost]
  -> Vcsrepo["/var/www/vhosts/${vhost}/"]
}
