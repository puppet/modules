# Class to do base config for mysql server
#
class profile::mysqlserver (
  String $rootpw,
  Hash   $override_options = {},
){
  include ::profile::domaincerts

  class { '::mysql::server':
    root_password    => $rootpw,
    override_options => $override_options
  }

  -> file { '/etc/mysql/debian.cnf':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    content => template('profile/mysqlserver/debian.cnf.erb'),
  }

  user { 'mysql':
    ensure => present,
    groups => [ 'ssl-cert' ],
  }
}
