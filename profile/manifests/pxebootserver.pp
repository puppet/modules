# Class to manage TFTP pxeboot server
#
class profile::pxebootserver(
  String $dir = '/opt/tftp',
  String $options = '--timeout 60 --secure',
) {
  file { $dir:
    ensure => directory,
  }

  class { 'tftp':
    directory => $dir,
    address   => $facts['networking']['ip'],
    options   => $options,
    inetd     => false,
  }
}
