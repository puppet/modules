# Class: profile::gitlab
#
# Custom config files to support a gitlab install
#
class profile::gitlab {
  include ::systemd

  file {
    default:
      ensure => file;
    '/etc/ssh/sshd_config.d/port.conf':
      content => "Port 33\n",
      notify  => Service['sshd'];
    '/etc/fail2ban/jail.d/custom-sshd.conf':
      content => "[sshd]\nport = 33",
      notify  => Service['fail2ban'];
  }

  systemd::dropin_file { 'ssh_port.conf':
    unit    => 'ssh.socket',
    content => "[Socket]\nListenStream=\nListenStream=33\n",
    notify  => Service['sshd']
  }

  service { 'sshd':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }

  service { 'fail2ban':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }
}