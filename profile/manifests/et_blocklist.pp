# Class: profile::et_blocklist
#
class profile::et_blocklist {
  include ::firewall
  include ::profile::base::insomnia247_dir

  firewallchain { 'et-blocklist:filter:IPv4':
    ensure  => present,
  }

  firewall { '010 Run through ET blocklist':
    chain => 'INPUT',
    proto => 'all',
    jump  => 'et-blocklist',
  }

  cron { 'et_blocklist':
    command  => '/usr/bin/ruby /opt/insomnia247/et-blocklist/cronjob.rb',
    user     => 'root',
    month    => '*',
    monthday => '*',
    hour     => '0',
    minute   => '0',
  }

  file {
    default:
      owner => 'root',
      group => 'root',
      mode  => '0750';
    '/opt/insomnia247/et-blocklist':
      ensure => directory;
    '/opt/insomnia247/et-blocklist/cronjob.rb':
      ensure => file,
      source => 'puppet:///modules/profile/et_blocklist/cronjob.rb';
  }
}
