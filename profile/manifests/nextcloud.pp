# Class: profile::nextcloud
#
# Prepares server for a nextcloud install.
# The installation/config etc itself is still a manual process right now.
#
class profile::nextcloud (
  String $domain,
  String $db_pass,
  String $db_name = 'nextcloud',
  String $datadir = '/home/nextcloud/',
) {
  include ::profile::mysqlserver
  include ::profile::redis
  include ::site_apache

  file { $datadir:
    ensure => directory,
    owner  => 'www-data',
    group  => 'www-data',
    mode   => '0750',
  }

  # Configure database
  mysql::db { $db_name:
    ensure   => present,
    user     => $db_name,
    password => $db_pass,
    charset  => 'utf8mb3',
    collate  => 'utf8mb3_general_ci',
    grant    => ['ALL'],
  }

  # Configure vhost
  site_apache::vhost::system { $domain:
    strongtls     => true,
    requirelimits => false,
  }

  # Add php opcache settings
  file { '/etc/php/8.2/apache2/conf.d/30-nextcloud.ini':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/profile/nextcloud/php.ini',
  }

  # Add cronjob for automatic updates
  cron {
    default:
      environment => 'MAILTO=""',
      user        => 'www-data',
      hour        => '5';
    'nextcloud_update':
      command => '/usr/bin/php /var/www/vhosts/nextcloud.insomnia247.nl/updater/updater.phar -n',
      minute  => '30';
    'nextcloud_apps_update':
      command => '/usr/bin/php /var/www/vhosts/nextcloud.insomnia247.nl/occ app:update --all',
      minute  => '15';
  }
}
