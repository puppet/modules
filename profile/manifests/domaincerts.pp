# Class to provision shared domain certificate
#
class profile::domaincerts(
  String $ca,
  String $key,
  String $cert,
  String $basepath   = '/etc/ssl',
  String $certfile   = "${basepath}/certs/insomnia247-nl.crt",
  String $keyfile    = "${basepath}/private/insomnia247-nl.pem",
  String $cafile     = "${basepath}/certs/comodo-intermediate.crt",
  String $bundlefile = "${basepath}/certs/insomnia247-nl_bundle.crt",
) {
  include ::profile::base::dhparams

  file { $basepath:
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755'
  }
  -> file {
    default:
      ensure => directory,
      owner  => 'root',
      group  => 'ssl-cert';
    "${basepath}/certs":
      mode => '0755';
    "${basepath}/private":
      mode => '0750';
  }
  -> file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'ssl-cert';
    $cafile:
      mode    => '0644',
      content => $ca;
    $certfile:
      mode    => '0440',
      content => Sensitive($cert);
    $keyfile:
      mode    => '0440',
      content => Sensitive($key);
    $bundlefile:
      mode    => '0440',
      content => Sensitive("${cert}${ca}");
  }

  Class['::profile::domaincerts'] -> Class['::profile::base::dhparams']
}
