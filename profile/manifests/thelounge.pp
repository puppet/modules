# Profile to install and configure a webchat
#
class profile::thelounge (
  String $configfile = '/home/thelounge/.thelounge/config.js',
  String $webircpass = '',
){
  include ::profile::domaincerts

  # Set up repo and install nodejs
  class { 'nodejs':
    repo_version => '20',
  }

  package { 'lsb-release':
    ensure => installed,
  }

  # Install thelounge itself
  -> package { 'thelounge':
    ensure   => latest,
    provider => 'npm',
    notify   => Service['thelounge'],
  }

  # Create user to run under
  user { 'thelounge':
    ensure  => present,
    comment => 'User to run thelounge',
    home    => '/home/thelounge',
    shell   => '/bin/bash',
    groups  => ['ssl-cert']
  }

  # Write config file
  -> file { ['/home/thelounge', '/home/thelounge/.thelounge']:
    ensure => directory,
    owner  => 'thelounge',
    group  => 'thelounge',
    mode   => '0750',
  }

  -> file { $configfile:
    ensure  => file,
    owner   => 'thelounge',
    group   => 'thelounge',
    content => template('profile/thelounge/config.js.erb'),
  }

  # Manage service file
  file { '/etc/systemd/system/thelounge.service':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/profile/thelounge/thelounge.service',
  }
  ~> exec { 'myservice-systemd-reload':
    command     => 'systemctl daemon-reload',
    path        => [ '/usr/bin', '/bin', '/usr/sbin' ],
    refreshonly => true,
  }

  # Start service
  service { 'thelounge':
    ensure  => running,
    enable  => true,
    require => File['/etc/systemd/system/thelounge.service'],
  }

  # Ensure ordering
  Class['::profile::domaincerts'] ~> Service['thelounge']
  Package['lsb-release']          -> Package['thelounge']
  Package['thelounge']            -> Service['thelounge']
  File[$configfile]               -> Service['thelounge']
}
