# Class to set up LetsEncrypt config
#
class profile::base::letsencrypt {
  class { '::letsencrypt':
    config => {
      email  => 'certificates@insomnia247.nl',
      server => 'https://acme-staging.api.letsencrypt.org/directory',
    }
  }
}
