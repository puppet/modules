# Class to generate fresh dhparams
#
# @size bitsize for dhparams
#
class profile::base::dhparams (
  Integer $bitsize = 2048,
) {
  include ::profile::base::packages

  exec { 'dhparamsfile':
    command => "openssl dhparam -outform PEM -2 ${bitsize} > dhparams.pem",
    path    => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    creates => '/etc/ssl/dhparams.pem',
    cwd     => '/etc/ssl',
  }

  -> file { '/etc/ssl/dhparams.pem':
    ensure => file,
    owner  => 'root',
    group  => 'ssl-cert',
    mode   => '0750',
  }

  Class['::profile::base::packages'] -> Class['::profile::base::dhparams']
}
