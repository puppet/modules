# Class to check if puppet runs produced errors
#
# @param apikey API key for nanobot to send notifications
# @param use_gem Toggle used to specify if we are using an os package or the ruby gem
#
class profile::base::puppetcheck(
  $apikey,
) {
  include ::profile::base::insomnia247_dir
  include ::profile::base::packages

  file {
    default:
      ensure => 'directory',
      owner  => 'root',
      group  => 'root',
      mode   => '0640';
    '/opt/insomnia247/puppetcheck/':
      ;
    '/opt/insomnia247/puppetcheck/puppetcheck.rb':
      ensure => 'file',
      source => 'puppet:///modules/profile/base/puppetcheck.rb';
  }

  $ruby_path = '/opt/puppetlabs/puppet/bin/ruby'
  # $ruby_path = $profile::base::puppetagent::use_gem ? {
  #   true    => '/usr/bin/ruby',
  #   default => '/opt/puppetlabs/puppet/bin/ruby',
  # }

  cron { 'puppetcheck':
    command     => "${ruby_path} /opt/insomnia247/puppetcheck/puppetcheck.rb",
    environment => "APIKEY=${apikey}",
    user        => 'root',
    month       => '*',
    monthday    => '*',
    hour        => [9, 21],
    minute      => 10,
  }

  Class['profile::base::insomnia247_dir']
  -> File['/opt/insomnia247/puppetcheck']
}
