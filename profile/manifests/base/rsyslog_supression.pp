# Class: profile::base::rsyslog_supression
#
#
class profile::base::rsyslog_supression (
  Array[String] $messages = ['Generic placeholder message that does not exist'],
  Array[String] $programs = ['placeholderprogram'],
) {
  file {
    default:;
    '/etc/rsyslog.d':
      ensure => directory,
      mode   => '0755';
    '/etc/rsyslog.d/message_supression.conf':
      ensure  => file,
      mode    => '0644',
      content => inline_template('<%= @messages.map{ |m| ":msg, contains, \"#{m}\" stop" }.join("\n") %>');
    '/etc/rsyslog.d/program_supression.conf':
      ensure  => file,
      mode    => '0644',
      content => inline_template('<%= @programs.map{ |m| ":programname, isequal, \"#{m}\" stop" }.join("\n") %>');
  }

  ~> service { 'rsyslog':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }
}
