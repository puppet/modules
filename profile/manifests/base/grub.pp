# Class to manage grub config
#
# Used to set low timeout and disable the default "quiet" and "spash" options
#
class profile::base::grub (
  Boolean $custom                = false,
  Optional[String] $force_cgroup = '',
) {
  if $custom {
    file { '/etc/default/grub':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('profile/grub/grub.cfg.erb'),
    }
    ~> exec { 'grub_update-grub':
      command     => 'update-grub',
      path        => [ '/usr/bin', '/bin', '/usr/sbin' ],
      refreshonly => true,
    }
  }
}
