# Helper class to do some permission hardening for files
# and directories we have no interest in puppet managing
# otherwise.
#
class profile::base::hardening {
  file {
    default:
      ensure => directory;
    '/boot/grub/grub.cfg':
      ensure => file,
      mode   => '0400';
    '/etc/crontab':
      ensure => file,
      mode   => '0600';
    '/etc/cron.hourly':
      mode => '0700';
    '/etc/cron.daily':
      mode => '0700';
    '/etc/cron.weekly':
      mode => '0700';
    '/etc/cron.monthly':
      mode => '0700';
    '/etc/cron.d':
      mode => '0700';
    '/etc/passwd-':
      ensure => file,
      mode   => '0600';
    '/etc/shadow-':
      ensure => file,
      mode   => '0600';
    '/etc/group-':
      ensure => file,
      mode   => '0600';
    '/etc/gshadow-':
      ensure => file,
      mode   => '0600';
  }
}
