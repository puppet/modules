# Class to enable BBR algorithm for TCP congestion control
#
# Currently this requires default_qdisc to be fq, after
# kernel 4.13 fq_codel may be used with bbr.
#
class profile::base::tcp_bbr {
  if $facts['virtual'] == 'kvm' or
    $facts['virtual']  == 'virtualbox' or
    $facts['virtual']  == 'physical' {

    if versioncmp($facts['kernelmajversion'], '4.9') < 0 {
      # Kernel older than 4.9
      sysctl { 'net.core.default_qdisc': value => 'fq_codel' }

    } elsif versioncmp($facts['kernelmajversion'], '4.13') < 0 {
      # Kernel 4.9 to 4.12
      sysctl { 'net.core.default_qdisc': value => 'fq' }
      sysctl { 'net.ipv4.tcp_congestion_control': value => 'bbr' }

    } else {
      # Kernel 4.13 and newer
      sysctl { 'net.core.default_qdisc': value => 'fq_codel' }
      sysctl { 'net.ipv4.tcp_congestion_control': value => 'bbr' }
    }
  }
}
