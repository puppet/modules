# Class: profile::base::insomnia247_dir
#
# Create /opt/insomnia247
#
class profile::base::insomnia247_dir {
  file { '/opt/insomnia247/':
    ensure => directory,
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }
}
