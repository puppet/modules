# Class to set /etc/mailname
#
# @mailname system mailname
#
class profile::base::mailname (
  String $mailname = $facts['networking']['fqdn'],
) {
  file { '/etc/mailname':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => $mailname,
  }
}
