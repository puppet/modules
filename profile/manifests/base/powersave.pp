# Class to tweak some power settings for specific hardware.
# Currently only supports settings done by changes to /proc/sys etc.
#
class profile::base::powersave (
  Hash $procsys = {},
  Hash $sysfs   = {},
) {
  include ::sysfs

  each($procsys) |String $key, String $value| {
    sysctl { $key:
      value => $value,
    }
  }

  each($sysfs) |String $key, String $value| {
    sysfs::setting { $key:
      value => $value,
    }
  }
}
