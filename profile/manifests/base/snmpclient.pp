# Set up snmp client
#
# @param location    System location marker
# @param netblock    Netblock that is allowed to query the SNMP client
# @param rocommunity Read-only community name
#
class profile::base::snmpclient(
  # String $rocommunity,
  # String $contact  = 'coolfire@insomnia247.nl',
  # String $location = 'Amsterdam, Netherlands',
  # String $netblock = '10.0.1.116',
) {
  # class { 'snmp':
  #   agentaddress  => [ 'udp:161' ],
  #   ro_community  => $rocommunity,
  #   ro_community6 => $rocommunity,
  #   com2sec       => [ "notConfigUser default ${rocommunity}" ],
  #   com2sec6      => [ "notConfigUser default ${rocommunity}" ],
  #   ro_network    => $netblock,
  #   contact       => $contact,
  #   location      => $location,
  # }

  # if $facts['os']['family'] == 'Debian' and versioncmp($facts['os']['release']['major'], '9') >= 0 {
  #   file { '/etc/systemd/system/snmpd.service':
  #     ensure => absent,
  #   }
  #   #   ensure => file,
  #   #   owner  => 'root',
  #   #   group  => 'root',
  #   #   mode   => '0755',
  #   #   source => 'puppet:///modules/profile/snmpclient/snmpd-deb.service',
  #   # }
  #   # ~> exec { 'snmpd-systemd-reload':
  #   #   command     => 'systemctl daemon-reload',
  #   #   path        => [ '/usr/bin', '/bin', '/usr/sbin' ],
  #   #   refreshonly => true,
  #   # }
  # }
}
