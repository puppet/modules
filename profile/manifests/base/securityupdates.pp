# Class to automatically install security updates
#
# @param all_repos Enable to allow automatic updates from all repos rather than just the default OS repos
#
class profile::base::securityupdates (
  Boolean $all_repos = true,
) {
  if $facts['os']['family'] == 'debian' {
    package { 'unattended-upgrades':
      ensure => 'installed'
    }

    if $all_repos {
      file { '/etc/apt/apt.conf.d/50unattended-upgrades':
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => 'puppet:///modules/profile/base/apt-unattended-upgrades'
      }
    }

    cron { 'unattended-upgrades':
      command     => '/usr/bin/apt-get update && /usr/bin/unattended-upgrades',
      user        => 'root',
      month       => '*',
      monthday    => '*',
      hour        => '*',
      environment => ['MAILTO=""','PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'],
      minute      => fqdn_rand(60),
    }
  }
}
