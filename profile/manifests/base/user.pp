# Class: profile::base::user
#
#
class profile::base::user(
  Hash  $users,
  Array $groups = ['sudo'],
) {
  include sudo

  $users.each |$user, $config| {
    user { $user:
      ensure     => present,
      groups     => $groups,
      home       => "/home/${user}",
      shell      => $config['shell'],
      password   => $config['password'],
      managehome => true,
    }

    -> file { "/home/${user}/.ssh":
      ensure => directory,
      owner  => $user,
      group  => $user,
      mode   => '0750',
    }

    -> ssh_authorized_key { $config['key']['owner']:
      ensure => present,
      user   => $user,
      type   => $config['key']['type'],
      key    => $config['key']['data'],
    }
  }
}
