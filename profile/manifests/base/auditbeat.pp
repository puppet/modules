# Install and configure auditbeat
#
class profile::base::auditbeat (
  String $es_user,
  String $es_pass,
  String $es_index,
  String $es_host,
  String $es_ca_fp,
  Array[String] $suppress = [],
  Boolean $enable = false,
) {
  if $enable {
    if $facts['virtual'] == 'kvm' or
    $facts['virtual'] == 'virtualbox' or
    $facts['virtual'] == 'physical' {
      include apt

      # Add repo and key
      apt::source { 'elastic_8':
        location => 'https://artifacts.elastic.co/packages/8.x/apt',
        comment  => 'Elastic 8.x artifacts repo',
        repos    => 'main',
        release  => 'stable',
        key      => {
          id     => '46095ACC8548582C1A2699A9D27D666CD88E42B4',
          source => 'https://artifacts.elastic.co/GPG-KEY-elasticsearch',
        },
      }

      # Install package
      -> package { 'auditbeat':
        ensure => latest,
      }

      # Service management
      -> service { 'auditbeat':
        ensure     => running,
        enable     => true,
        hasrestart => true,
      }

      # Write config file
      file { '/etc/auditbeat/auditbeat.yml':
        ensure  => file,
        mode    => '0600',
        notify  => Service['auditbeat'],
        content => template('profile/base/auditbeat.yml.erb'),
      }

      service { 'systemd-journald-audit.socket':
        ensure => stopped,
        enable => mask,
      }

      Package['auditbeat'] -> File['/etc/auditbeat/auditbeat.yml']
    }
  }
}
