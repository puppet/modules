# Class to allow larger than normal network buffers
# since we are not strapped for memory and the use
# of tcp bbr will make sure it is only used if needed.
#
class profile::base::network_buffers {
  if $facts['virtual'] == 'kvm' or
    $facts['virtual']  == 'virtualbox' or
    $facts['virtual']  == 'physical' {

    if versioncmp($facts['kernelmajversion'], '4.9') >= 0 {
      # Kernel 4.9 and newer support bbr so we can increase buffers
      sysctl { 'net.core.wmem_max': value => '16777216' }
      sysctl { 'net.ipv4.tcp_wmem': value => "4096\t16384\t16777216" }
      sysctl { 'net.core.rmem_max': value => '25165824' }
      sysctl { 'net.ipv4.tcp_rmem': value => "4096\t87380\t25165824" }
    }
  }
}
