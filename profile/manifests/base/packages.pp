# Class to install misc packages that do not belong to any
# specific puppet modules.
#
# Package list is built by using a deep merge on 'profile::base::packages'.
#
class profile::base::packages{
  $packages = lookup('profile::base::packages', Array, 'unique', [])

  package { $packages:
    ensure => 'installed'
  }
}
