# Class to manage the timing of puppet-agent
#
# @param args   Arguments to pass to puppet agent when running
# @param hostip IP address to set for host 'puppet'
# @param offset Minutes after every hour and half hour the agent will run
#
class profile::base::puppetagent(
  $args           = '--onetime --no-daemonize',
  $server         = 'puppet.insomnia247.nl',
  $offset         = fqdn_rand(30),
  $package        = 'puppet-agent',
  $wrapper        = false,
  $package_source = 'vendor',
  Hash $settings  = {},
) {
  include ::profile::base::puppetcheck

  $agent_path = $package_source ? {
    gem      => '/usr/local/bin/puppet',
    upstream => '/usr/bin/puppet',
    vendor   => '/opt/puppetlabs/puppet/bin/puppet',
  }

  $agent_command = $wrapper ? {
    true    => "/opt/insomnia247/puppetwrapper/run.rb ${agent_path} agent ${args} --server ${server}",
    default => "${agent_path} agent ${args} --server ${server}",
  }

  cron { 'puppetagent':
    command  => $agent_command,
    user     => 'root',
    month    => '*',
    monthday => '*',
    hour     => '*',
    minute   => [$offset, $offset + 30],
  }

  $clientbucket_path = $package_source ? {
    gem      => '/opt/puppetlabs/puppet/cache/clientbucket/',
    upstream => '/var/cache/puppet/clientbucket/',
    vendor   => '/opt/puppetlabs/puppet/cache/clientbucket/',
  }

  cron { 'clientbucket_cache_cleanup':
    command  => "/usr/bin/find ${clientbucket_path} -type f -mtime +45 -atime +45 -delete; \
    /usr/bin/find ${clientbucket_path} -type d -empty -delete",
    user     => 'root',
    month    => '*',
    monthday => '1',
    hour     => '4',
    minute   => '0',
  }

  service { ['puppet', 'mcollective']:
    ensure => stopped,
    enable => false,
  }

  $install_options = $package_source ? {
    gem      => {
      ensure   => latest,
      provider => 'gem',
    },
    upstream => {
      ensure => installed,
    },
    vendor   => {
      ensure => installed,
    },
  }

  package { $package:
    * => $install_options,
  }

  file {
    default:
      owner => 'root',
      group => 'root',
      mode  => '0750';
    '/opt/insomnia247/puppetwrapper':
      ensure => directory;
    '/opt/insomnia247/puppetwrapper/run.rb':
      ensure => file,
      source => 'puppet:///modules/profile/base/puppet-agent-wrapper.rb';
  }

  $ini_defaults = { 'path' => '/etc/puppetlabs/puppet/puppet.conf' }
  inifile::create_ini_settings($settings, $ini_defaults)
}
