# Class: profile::base::timesyncd
#
# Use systemd-timesyncd to keep system clock in sync
#
class profile::base::timesyncd {
  include ::systemd

  package { 'ntp':
    ensure => purged,
  }
}
