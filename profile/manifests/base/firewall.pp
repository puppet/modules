# Class to configre iptables
#
class profile::base::firewall (
  Boolean $purge = true,
) {
  # We need an extra package to make sure rules can be saved
  include firewall

  # Purge any rules we didn't make
  resources { 'firewall':
    purge => $purge,
  }

  # Default rules IPv4
  firewall { '000 accept related established rules':
    proto => 'all',
    state => ['RELATED', 'ESTABLISHED'],
    jump  => 'accept',
  }
  -> firewall { '001 accept all icmp':
    proto => 'icmp',
    jump  => 'accept',
  }
  -> firewall { '002 accept all to lo interface':
    proto   => 'all',
    iniface => 'lo',
    jump    => 'accept',
  }
  -> firewall { '003 reject local traffic not on loopback interface':
    iniface     => '! lo',
    proto       => 'all',
    destination => '127.0.0.1/8',
    jump        => 'reject',
  }

  # Default rules IPv6
  firewall { '000 accept related established rules v6':
    proto    => 'all',
    state    => ['RELATED', 'ESTABLISHED'],
    jump     => 'accept',
    protocol => 'IPv6',
  }
  -> firewall { '001 accept all icmp v6':
    proto    => 'icmp',
    jump     => 'accept',
    protocol => 'IPv6',
  }
  -> firewall { '002 accept all to lo interface v6':
    proto    => 'all',
    iniface  => 'lo',
    jump     => 'accept',
    protocol => 'IPv6',
  }
  -> firewall { '003 reject local traffic not on loopback interface v6':
    iniface     => '! lo',
    proto       => 'all',
    destination => '::1/128',
    jump        => 'reject',
    protocol    => 'IPv6',
  }
}
