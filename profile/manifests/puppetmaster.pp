# Profile to manage some stuff on the puppetmaster itself
#
class profile::puppetmaster(
  String $reportsdir = '/opt/puppetlabs/server/data/puppetserver/reports/',
) {
  tidy { $reportsdir:
    age     => '10d',
    matches => '*.yaml',
    recurse => true,
    rmdirs  => false,
    type    => ctime,
  }
}
