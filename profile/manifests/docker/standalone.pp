# Docker swarm profile
#
# Profile that installs docker for a standalone node
#
class profile::docker::standalone (
  Boolean $manage_users = true,
) {
  include ::profile::base::packages
  include ::profile::base::securityupdates
  include ::site_ssh::keys_only

  if $manage_users {
    include ::profile::base::user
    include ::site_rundeck::client
  }

  cron {
    default:
      user     => 'root',
      month    => '*',
      monthday => '*',
      hour     => '00',
      minute   => '00';
    'docker-clean-containers':
      command => 'docker rm -v $(docker ps -a -q -f status=exited)';
    'docker-clean-images':
      command => 'docker image rm $(docker images -f "dangling=true" -q)';
    'docker-clean-overlays':
      command => 'docker rmi -f $(docker images -aq)';
    'docker-clean-volumes':
      command => 'docker volume rm $(docker volume ls -qf dangling=true)';
  }
}
