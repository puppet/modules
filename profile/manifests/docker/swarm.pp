# Docker swarm profile
#
# Profile that installs and manges a docker swarm
#
# @param manager Name of the node that is the manager
# @param token   Token required to join the docker swarm
#
class profile::docker::swarm (
  String $manager,
  String $token,
) {
  include profile::base::snmpclient
  include profile::base::user
  include site_rundeck::client
  include consul

  # Network driver issue dirty workaround untill upstream fixes
  cron { 'tg3_driver_workaround':
    command  => '/bin/ping -c 1 10.0.0.1 || /sbin/rmmod tg3 && /sbin/modprobe tg3',
    user     => 'root',
    month    => '*',
    monthday => '*',
    hour     => '*',
    minute   => '*',
  }

  # Consul requires unzip
  package { 'unzip':
    ensure => installed,
  }

  # Use the version provided by the ubuntu repos
  class { 'docker':
    version => 'latest',
  }

  # All nodes should run the swarm client
  docker::swarm { 'cluster_worker':
    join           => true,
    advertise_addr => $facts['networking']['ip'],
    listen_addr    => $facts['networking']['ip'],
    manager_ip     => $manager,
    token          => $token,
    require        => Class['docker'],
  }

  # Make the manager node the manager
  if $manager == $facts['networking']['fqdn'] {
    docker::swarm { 'cluster_manager':
      init           => true,
      advertise_addr => $facts['networking']['ip'],
      listen_addr    => $facts['networking']['ip'],
      require        => Class['docker'],
      before         => Docker::Swarm['cluster_worker'],
    }

    exec { 'wget_portainer_stack':
      command => '/usr/bin/wget https://downloads.portainer.io/portainer-agent-stack.yml -O /root/portainer-agent-stack.yml',
      creates => '/root/portainer-agent-stack.yml',
      require => Docker::Swarm['cluster_worker'],
    }

    -> docker::stack { 'portainer':
      ensure        => present,
      stack_name    => 'portainer',
      compose_files => ['/root/portainer-agent-stack.yml'],
    }
  }

  exec { 'consul_join':
    command   => "consul join ${manager} && touch /dev/shm/consul_join",
    path      => '/usr/local/bin/:/usr/bin/',
    require   => Class['consul'],
    before    => Class['docker'],
    tries     => 10,
    try_sleep => 1,
    creates   => '/dev/shm/consul_join',
  }

  cron {
    default:
      user     => 'root',
      month    => '*',
      monthday => '*',
      hour     => '00',
      minute   => '00';
    'docker-clean-containers':
      command => 'docker rm -v $(docker ps -a -q -f status=exited)';
    'docker-clean-images':
      command => 'docker rmi $(docker images -f "dangling=true" -q)';
    'docker-clean-volumes':
      command => 'docker volume rm $(docker volume ls -qf dangling=true)';
  }

  Package['unzip'] -> Class['consul'] -> Class['docker']
}
