# Class: profile::docker::managed_workloads
#
# Allows deployment and auto-update of docker-compose defined workloads from gitlab
# This class assumes one of your deployments is https://git.insomnia247.nl/coolfire/docker-updater
#
class profile::docker::managed_workloads(
  Hash[String, String] $keyscan_hosts,
  String $deploy_key,
  Hash[String, Hash]   $deployments,
  String $basepath    = '/srv/docker-deployments',
  String $compose_cmd = 'docker-compose',
) {
  file {
    default:
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0755';
    $basepath:
      recurse      => true,
      recurselimit => 1;
    '/opt/insomnia247/keyscans':;
    '/root/.ssh/deploy_key':
      ensure  => file,
      mode    => '0600',
      content => Sensitive($deploy_key);
  }

  # preseed ssh host keys for git ssh commands
  $keyscan_hosts.each |String $hostname, String $port| {
    exec { "Ssh-keyscan for ${hostname}:${port}":
      command => "/usr/bin/ssh-keyscan -p ${port} ${hostname} >> /root/.ssh/known_hosts\
       && touch /opt/insomnia247/keyscans/${hostname}:${port}",
      creates => "/opt/insomnia247/keyscans/${hostname}:${port}",
    }
  }

  # Supported docker-compose filenames
  $compose_files = ['docker-compose.yml', 'docker-compose.yaml', 'compose.yml', 'compose.yaml']

  # Pull in deployments
  $deployments.each |String $name, Hash $args| {
    # Pull in the git repo
    vcsrepo { "${basepath}/${name}":
      ensure   => latest,
      provider => git,
      source   => $args['repo'],
      identity => '/root/.ssh/deploy_key',
    }

    # Deploy any extra secret files
    unless $args['files'] == undef {
      $args['files'].each |String $fname, String $content| {
        file { "${basepath}/${name}/${fname}":
          ensure  => file,
          mode    => '0600',
          content => Sensitive($content),
        }
        Vcsrepo["${basepath}/${name}"] -> File["${basepath}/${name}/${fname}"] -> Exec["Bring up ${name}"]
      }
    }

    # Make a list of all possible docker-compose file paths
    $compose_paths = $compose_files.map |$compose_file| {
      "${basepath}/${name}/${$compose_file}"
    }

    # Bring up the workload if it supports docker-compose
    exec { "Bring up ${name}":
      command     => "${compose_cmd} up -d",
      path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
      cwd         => "${basepath}/${name}",
      onlyif      => "test -f ${join($compose_paths, ' -o -f ')}",
      subscribe   => Vcsrepo["${basepath}/${name}"],
      refreshonly => true,
    }
  }

  # Create cronjob for auto-updates
  cron { 'docker-updater':
    command => '/usr/bin/ruby /srv/docker-deployments/docker-updater/updater.rb',
    user    => 'root',
    hour    => '*/4',
    minute  => fqdn_rand(60),
  }
}
