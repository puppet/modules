# Class: profile::opendkim
#
# Configure opendkim on host
#
class profile::opendkim (
  String $publickey,
  String $privatekey,
  String $domain = 'insomnia247.nl',
  String $selector = 'mail'
) {
  class { '::opendkim':
    socket        => 'inet:8891@127.0.0.1',
    trusted_hosts => ['::1','127.0.0.1','localhost'],
    keys          => [{
      domain         => $domain,
      selector       => $selector,
      publickey      => $publickey,
      privatekey     => $privatekey,
      signingdomains => ["*@${domain}"],
    }]
  }
}
