# Class to manage mongodb server
#
class profile::mongodb (
  String $admin_password,
  String $admin_username  = 'root',
  String $version         = '4.2.2',
  String $repo_key        = 'E162F504A20CDF15827F718D4B7C549A058F8B6B',
  String $repo_key_server = 'hkp://keyserver.ubuntu.com:80',
){

  # This is a workaround since the upstream puppet module did not yet merge the 4.2 repo keys
  apt::source { 'mongodb-workaround':
    location => 'https://repo.mongodb.org/apt/debian',
    release  => 'buster/mongodb-org/4.2',
    repos    => 'main',
    key      => {
      id     => $repo_key,
      server => $repo_key_server,
    },
  }

  -> class {'mongodb::globals':
    manage_package_repo => false,
    manage_package      => true,
    version             => $version,
  }

  -> class { 'mongodb::client': }

  -> class { 'mongodb::server':
    auth           => false,
    create_admin   => true,
    admin_username => $admin_username,
    admin_password => $admin_password,
    store_creds    => true,
    directoryperdb => true,
  }
}
