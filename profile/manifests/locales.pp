# Class to manage locales
#
# @locales locales to generate
#
class profile::locales (
  String $locales = "en_US.UTF-8 UTF-8\n",
) {
  include ::profile::base::packages

  file { '/etc/default/locale':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/profile/locales/etc-default-locale'
  }

  file { '/etc/locale.gen':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => $locales,
  }

  ~> exec { 'locale_gen':
    command     => 'locale-gen && update-locale',
    path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    refreshonly => true,
  }

  Class['::profile::base::packages'] -> Class['::profile::locales']
}
