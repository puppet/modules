# Class to manage DNS services
#
class profile::dnsserver (
  String $listenip = '10.0.0.1',
  Hash   $zones    = {},
  Array  $no_fwd   = [],
  Array  $rslv_ips = [],
){
  # Make sure the DNS server package is installed
  package { 'bind9':
    ensure => installed,
  }

  # Make sure the DNS daemon is running
  service { 'bind9':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
    require    => Package['bind9'],
  }

  # Generate reverse zones
  $_zones = generate_ptr($zones)

  # Manage zone files
  each($_zones) |String $zone, Hash $records| {
    file { "/etc/bind/db.${zone}":
      ensure  => file,
      content => template('profile/bind9_zone.erb'),
      notify  => Service['bind9'],
    }
  }

  # Manage zones that should be resolved by bind itself, rather than forwarded to the upstream DNS server
  if size($no_fwd) > 0 {
    file { '/etc/bind/named.conf.no_fwd':
      ensure  => file,
      content => template('profile/bind9_no_fwd.erb'),
      notify  => Service['bind9'],
    }
  }

  file { '/etc/bind/db.root':
    ensure => link,
    target => '/usr/share/dns/root.hints',
  }

  file { '/etc/bind/named.conf.default-zones':
      ensure  => file,
      content => template('profile/bind9_zones_conf.erb'),
      notify  => Service['bind9'],
  }

  file { '/etc/bind/named.conf.options':
    ensure  => file,
    content => template('profile/bind9_named.conf.options.erb'),
    notify  => Service['bind9'],
  }
}
