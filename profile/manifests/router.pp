# Router profile
#
class profile::router (
  String  $lan       = 'eth0',
  String  $wan       = 'eth1',
  Array   $rslv_ips  = ['9.9.9.9', '208.67.222.222', '208.67.220.220'],
  Array   $rslv_dom  = ['insomnia247.nl'],
  Hash    $forwards  = {},
  Array   $tr_blocks = [],
  Boolean $rundeck   = false,
){
  include profile::base::packages
  include profile::base::snmpclient
  include profile::base::tcp_bbr

  if $rundeck {
    include site_rundeck::client
  }

  # Override DNS resolver
  class { 'resolv_conf':
    nameservers => $rslv_ips,
    # searchpath  => $rslv_dom,
  }

  # Enable packet forwarding
  sysctl {
    'net.ipv4.ip_forward':
      value => '1';
    'net.netfilter.nf_conntrack_generic_timeout':
      value => '120';
    'net.ipv6.conf.all.forwarding':
      value => '1';
    'net.netfilter.nf_conntrack_max':
      value => '65536';
  }

  # NAT for wan
  firewall {
    default:
      chain    => 'POSTROUTING',
      jump     => 'MASQUERADE',
      proto    => 'all',
      outiface => $wan,
      table    => 'nat';
    '100 nat to wan':;
    '100 nat to wan v6':
      protocol => 'IPv6';
  }

  # forward packets along established/related connections
  # -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
  firewall {
    default:
      chain   => 'FORWARD',
      ctstate => ['RELATED', 'ESTABLISHED'],
      proto   => 'all',
      jump    => 'accept';
    '101 forward related and established':;
    '101 forward related and established v6':
      protocol => 'IPv6';
  }

  # forward from LAN (p1p1) to WAN (p4p1)
  # -A FORWARD -i p1p1 -o p4p1 -j ACCEPT
  -> firewall {
    default:
      chain    => 'FORWARD',
      proto    => 'all',
      iniface  => $lan,
      outiface => $wan,
      jump     => 'accept';
    '102 forward from LAN to WAN':;
    '102 forward from LAN to WAN v6':
      protocol => 'IPv6';
  }

  # Port forwarding rules
  each($forwards) | $name, $forward | {
    firewall { "200 ${name}":
      table   => 'nat',
      chain   => 'PREROUTING',
      jump    => 'DNAT',
      iniface => 'interface' in $forward ? {
        true    => $forward['interface'],
        default => $wan
      },
      proto   => 'protocol' in $forward ? {
        true    => $forward['protocol'],
        default => 'tcp'
      },
      source  => 'source' in $forward ? {
        true    => $forward['source'],
        default => undef
      },
      dport   => 'port' in $forward ? {
        true    => $forward['port'],
        default => undef
      },
      todest  => $forward['destination'],
    }

    if 'destination6' in $forward {
      firewall { "200 ${name} v6":
        table    => 'nat',
        chain    => 'PREROUTING',
        jump     => 'DNAT',
        iniface  => 'interface' in $forward ? {
          true    => $forward['interface'],
          default => $wan
        },
        proto    => 'protocol' in $forward ? {
          true    => $forward['protocol'],
          default => 'tcp'
        },
        source   => 'source6' in $forward ? {
          true    => $forward['source6'],
          default => '::/0'
        },
        dport    => 'port' in $forward ? {
          true    => $forward['port'],
          default => undef
        },
        todest   => $forward['destination6'],
        protocol => 'IPv6',
      }
    }
  }

  # Block certain hops from appearing for specific traceroute destinations
  # BPF bytecode does 'ip[8] < 3'
  each($tr_blocks) | $dest | {
    firewall { "080 ${dest}":
      chain       => 'FORWARD',
      bytecode    => '7,48 0 0 0,84 0 0 240,21 0 3 64,48 0 0 8,53 1 0 3,6 0 0 65535,6 0 0 0',
      proto       => 'all',
      jump        => 'drop',
      destination => $dest,
    }
  }
}
