# Class: profile::manager_cli
#
# Install dependencies and deploy
# manager2-cli repo
#
class profile::manager_cli {
  include ::profile::base::insomnia247_dir
  include ::profile::base::packages

  # Pull down repository
  vcsrepo { '/opt/insomnia247/manager2-cli':
    ensure   => latest,
    provider => git,
    user     => 'root',
    group    => 'root',
    source   => 'https://git.insomnia247.nl/shells/manager2-cli.git',
  }

  ~> exec { 'bundle_install':
    command     => '/usr/bin/bundle install',
    cwd         => '/opt/insomnia247/manager2-cli',
    user        => 'root',
    refreshonly => true,
  }

  file { '/usr/local/bin/manager':
    ensure => link,
    target => '/opt/insomnia247/manager2-cli/wrapper.sh'
  }

  Class['profile::base::packages']
  -> Vcsrepo['/opt/insomnia247/manager2-cli']
  -> File['/usr/local/bin/manager']

  Class['profile::base::insomnia247_dir']
  -> Vcsrepo['/opt/insomnia247/manager2-cli']
}
