# Class: profile::insomnia247nl
#
# Deploy insomnia247.nl site
#
class profile::insomnia247nl (
  String $deploy_dir = '/var/www/vhosts/insomnia247.nl',
){
  include ::profile::base::packages
  include ::profile::redis
  include ::site_apache

  # Pull down repository
  vcsrepo { $deploy_dir:
    ensure   => latest,
    provider => git,
    user     => 'www-data',
    group    => 'www-data',
    source   => 'https://git.insomnia247.nl/coolfire/insomnia247nl.git',
  }

  ~> exec { 'insomnia247nl_bundle_install':
    command     => '/usr/bin/bundle install',
    cwd         => $deploy_dir,
    user        => 'www-data',
    refreshonly => true,
  }

  Class['profile::base::packages']
  -> Vcsrepo[$deploy_dir]

  Class['site_apache']
  -> Vcsrepo[$deploy_dir]
}
