# Class: profile::radvdserver
#
#
class profile::radvdserver (
  String $interface = 'eth0'
) {
  # Install package
  package { ['radvd', 'radvdump']:
    ensure => installed,
  }

  # Make sure DHCP daemon is running
  service { 'radvd':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
    require    => Package['radvd'],
  }

  # Manage config
  file { '/etc/radvd.conf':
    ensure  => file,
    content => template('profile/radvd.conf.erb'),
    notify  => Service['radvd'],
  }
}
