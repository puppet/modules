
# Special case profile for Lydia.
#
# @param apache_system_vhosts Hash of vhosts that don't belong to any user
# @param apache_system_proxies Hash of system proxy vhosts
# @param apache_system_passenger Hash of system passenger apps
# @param users Hash of users and all their special properties
#
class profile::lydia (
  Hash  $apache_system_vhosts    = {},
  Hash  $apache_system_proxies   = {},
  Hash  $apache_system_passenger = {},
  Hash  $users                   = {},
) {
  include ::profile::base::firewall
  include ::profile::base::hardening
  include ::profile::base::packages
  include ::profile::base::rsyslog_supression
  include ::profile::base::snmpclient
  include ::profile::base::tcp_bbr
  include ::profile::deb_backports
  include ::profile::domaincerts
  include ::profile::et_blocklist
  include ::profile::insomnia247nl
  include ::profile::ldap_client
  include ::profile::locales
  include ::profile::lydia::outbound_mail_firewall
  include ::profile::manager_cli
  include ::profile::mysqlserver
  include ::profile::nextcloud
  include ::profile::opendkim
  #include ::profile::sshtunnel::destination
  include ::site_adminer
  include ::site_apache
  include ::site_apache::debian_logs
  include ::site_apache::logrotate_final
  #include ::site_apache::pagespeed
  include ::site_apache::php
  include ::site_apparmor
  include ::site_cgroups
  include ::site_cgroups::adhoc
  include ::site_dovecot
  include ::site_imagemagick
  include ::site_kernelcare
  include ::site_limits
  include ::site_motd
  include ::site_oidentd
  include ::site_podman
  include ::site_postfix
  include ::site_proftpd
  include ::site_quota
  include ::site_rundeck::client
  include ::site_spamassassin
  include ::site_ssh
  include ::site_ssh::client
  include ::site_tor
  include ::systemd

  # Install and manage nodejs repo
  class { 'nodejs':
    repo_version => '20',
  }

  # Install and manage fail2ban
  class { 'fail2ban': }

  -> file { '/etc/fail2ban/paths-overrides.local':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => "[DEFAULT]\ndovecot_log = /var/log/mail.log\n"
  }

  firewallchain { 'f2b-master:filter:IPv4':
    ensure => present,
  }

  -> firewall { '005 fail2ban master chain':
    proto => 'all',
    jump  => 'f2b-master',
  }

  firewallchain { 'f2b-master:filter:IPv6':
    ensure => present,
  }

  -> firewall { '005 fail2ban master chain v6':
    proto    => 'all',
    jump     => 'f2b-master',
    protocol => 'IPv6',
  }

  file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0750';

    # Create special backups directory
    '/home/backups':
      ensure => directory;

    # Create suwww script
    '/usr/bin/suwww':
      mode    => '0755',
      content => "#!/bin/bash\nsudo /bin/su \$USER-www -s /bin/bash\n";

    # Create file to export default java options
    '/etc/profile.d/80-java_options.sh':
      mode    => '0744',
      content => 'export _JAVA_OPTIONS="-Xms64m -Xmx512m -XX:ParallelGCThreads=2"';

    # Create file to export default cargo options
    '/etc/profile.d/80-cargo_options.sh':
      mode    => '0744',
      content => 'export CARGO_BUILD_JOBS=8';

    # Manage permissions for wall and write
    '/usr/bin/bsd-write':
      ;
    '/usr/bin/wall':
      ;

    # Create skeleton bash profile
    '/etc/skel/.bash_profile':
      mode   => '0644',
      source => 'puppet:///modules/profile/lydia/bash_profile';

    # Create skeleton bashrc with extra colour options
    '/etc/skel/.bashrc':
      mode   => '0644',
      source => 'puppet:///modules/profile/lydia/bashrc';

    # Create script for 'port' command
    '/usr/bin/port':
      mode   => '0755',
      source => 'puppet:///modules/profile/lydia/port.sh';

    # Create htoprc default
    '/etc/htoprc':
      mode   => '0644',
      source => 'puppet:///modules/profile/lydia/htoprc';

    # Make sure (al)pine uses correct domain name
    '/etc/pine.conf':
      mode    => '0644',
      content => 'user-domain=insomnia247.nl';

    # Make sure loginctl user-status does not bypass hidepid
    '/etc/dbus-1/system.d/systemd-restrict.conf':
      mode   => '0644',
      source => 'puppet:///modules/profile/lydia/systemd-restrict.conf';
  }

  # Manage mounts
  mount { '/proc':
    ensure  => present,
    device  => 'proc',
    fstype  => 'procfs',
    options => 'hidepid=2',
  }

  mount { '/dev/shm':
    ensure  => present,
    device  => 'tmpfs',
    fstype  => 'tmpfs',
    options => 'rw,nosuid,nodev,noexec',
  }

  # Take over wtmp logrotate
  logrotate::rule { 'wtmp':
    path         => '/var/log/wtmp',
    rotate       => 14,
    rotate_every => 'month',
    missingok    => true,
    create       => true,
    create_mode  => '0664',
    create_owner => 'root',
    create_group => 'utmp',
  }

  # Configure LE class
  class { '::letsencrypt':
    config => {
      email => 'certmaster@insomnia247.nl',
    }
  }

  # Per-user config
  create_resources('::profile::lydia::user', $users)

  # Create system vhosts
  create_resources('::site_apache::vhost::system', $apache_system_vhosts)
  create_resources('::site_apache::vhost::system_proxy', $apache_system_proxies)

  # Create shellinabox class
  class { 'site_shellinabox':
    domain => 'shell.insomnia247.nl',
  }

  # Stop some undesirable default services
  service{ 'inetd':
    ensure    => 'stopped',
    enable    => false,
    hasstatus => true,
  }

  service{ 'minissdpd':
    ensure    => 'stopped',
    enable    => false,
    hasstatus => true,
  }

  # Resrouce ordering constraints
  Class['::profile::base::packages']
  -> Class['::profile::ldap_client']
  -> Class['::site_ssh']
  -> ::Profile::Lydia::User<| |>

  Class['::site_motd']
  -> Class['::site_ssh']

  Class['::profile::ldap_client']
  -> ::Site_apache::Vhost::System<| |>

  Class['::profile::ldap_client']
  -> ::Site_apache::Vhost::System_proxy<| |>

  Class['::profile::ldap_client']
  -> Class['::site_cgroups']

  Class['::profile::domaincerts'] -> Class['::site_apache']
  Class['::profile::domaincerts'] -> Class['::site_dovecot']
  Class['::profile::domaincerts'] -> Class['::site_postfix']
  Class['::profile::domaincerts'] -> Class['::site_proftpd']

  Class['::profile::ldap_client'] -> Class['::profile::lydia::outbound_mail_firewall']
}
