# Class: profile::suricata
#
# Install and configure suricata network monitoring point
#
# @param interfaces Text list of interfaces suricata should process packets from
# @param oinkmaster_inline Array of strings that enable/disable/modify rules
# @param service_ensure Should the service be running or stopped?
#
class profile::suricata (
  String $interfaces        = 'eth0',
  Array  $oinkmaster_inline = [],
  String $service_ensure    = 'running',
) {
  include systemd

  class { '::suricata':
    interfaces     => $interfaces,
    user           => 'suricata',
    service_ensure => $service_ensure,
  }

  package { 'suricata-oinkmaster':
    ensure => installed,
  }

$cronjob_content = @(END)
#!/bin/bash
set +e

/usr/sbin/suricata-oinkmaster-updater
/usr/bin/suricatasc -c reload-rules /var/run/suricata/suricata.socket
END

  file { '/etc/cron.daily/suricata-oinkmaster':
    ensure  => file,
    content => $cronjob_content,
  }

  file { '/var/run/suricata':
    ensure => directory,
    owner  => 'suricata',
  }

$oinkmaster_template = @(END)
skipfile local.rules
skipfile deleted.rules
skipfile snort.conf
use_external_bins = 0

url = https://rules.emergingthreats.net/open/suricata-4.0.0/emerging.rules.tar.gz
<% @oinkmaster_inline.each do |rule| %>
<%= rule -%>
<% end %>
END

  file { '/etc/suricata/suricata-oinkmaster.conf':
    ensure  => file,
    content => inline_template($oinkmaster_template),
  }

$logrotate_file = @(END)
/var/log/suricata/*.log
{
  rotate 14
  missingok
  compress
  delaycompress
  copytruncate
  sharedscripts
  postrotate
    /bin/kill -HUP $(cat /var/run/suricata.pid)
  endscript
}
/var/log/suricata/*.json
{
  rotate 5
  maxsize 1G
  missingok
  compress
  delaycompress
  copytruncate
  sharedscripts
  postrotate
    /bin/kill -HUP $(cat /var/run/suricata.pid)
  endscript
}
END

  file { '/etc/logrotate.d/suricata':
    ensure  => file,
    mode    => '0644',
    content => $logrotate_file,
  }
}
