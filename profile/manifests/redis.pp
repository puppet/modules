# Class: profile::redis
#
# Set up local redis cache
#
class profile::redis (
  String $socketfile = '/var/run/redis/redis.sock',
) {
  # Set up and configure redis
  class { '::redis':
    bind           => '127.0.0.1',
    port           => 0,
    unixsocket     => $socketfile,
    unixsocketperm => '770',
  }

  # Allow www-data user to access our socket file
  user { 'www-data': }
  User <| title == 'www-data' |> { groups +> 'redis' }
}
