# Class: profile::influxdb
#
class profile::influxdb (
  String $hostname = $facts['networking']['fqdn'],
) {
  include ::profile::base::packages

  file { '/etc/influxdb/influxdb.conf':
    ensure  => file,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    notify  => Service['influxdb.service'],
    content => template('profile/influxdb.conf.erb'),
  }

  service { 'influxdb.service':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }

  resources { 'firewall':
    purge => true,
  }

  firewall { '010 Accept localhost':
    proto   => 'all',
    iniface => 'lo',
    action  => 'accept',
  }

  firewall { '012 related-established':
    proto  => 'all',
    state  => ['RELATED', 'ESTABLISHED'],
    action => 'accept',
  }

  firewall_multi { '020 Allow access to influxdb':
    source => [
      '10.0.0.100',
      '10.0.0.103',
      '10.0.1.116',
    ],
    dport  => [8083, 8086, 8088],
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '030 Drop any other attempts to acces influxdb':
    proto  => 'tcp',
    dport  => [8083, 8086, 8088],
    action => 'drop',
  }

  Class['::profile::base::packages']
  -> File['/etc/influxdb/influxdb.conf']
}
