# Class: profile::cacheproxy
#
# Deploy nginx as a reverse and caching proxy
#
# @param vhost hostname for proxy server
# @param backend server where requests need to be proxied to
# @param logrotate_su Enable to allow default "su" users for logrotate
# @param forcecache should the cache proxy ignore cache headers and cache anyway
# @param cachedir filesystem path where cache files should be stored
# @param keysize max memory to use for cache keys
# @param cachesize max storage to be used for cache
# @param maxlogsize max size for nginx log files
#
class profile::cacheproxy (
  String $vhost,
  String $backend,
  Boolean $logrotate_su = false,
  Boolean $forcecache   = false,
  String $cachedir      = '/cache',
  String $keysize       = '100m',
  String $cachesize     = '35g',
  String $maxlogsize    = '100M',
) {
  include profile::base::packages
  include systemd

  # Create directory where cached content should be stored
  file { $cachedir:
    ensure => directory,
    owner  => 'www-data',
    group  => 'www-data',
    mode   => '0700',
  }

  # Set up nginx with custom cache settings and log formats
  -> class { 'nginx':
    http_cfg_append => {
      'proxy_cache_path' => "${cachedir} levels=1:2 keys_zone=backendcache:${keysize} inactive=365d max_size=${cachesize}",
      'log_format'       => "custom_cache_log '[\$time_local] [Cache:\$upstream_cache_status] [\$host] [Remote_Addr: \$remote_addr] - \
\$remote_user - \$server_name to: \$upstream_addr: \"\$request\" \$status \$body_bytes_sent \"\$http_referer\" \"\$http_user_agent\"'"
    },
  }

  # If we choose, we can ignore headers that normally control caching rules
  $ignoredheaders = $forcecache ? {
    true    => ['Cache-Control', 'Expires', 'Set-Cookie'],
    default => ['X-Placeholder-Header'],
  }

  # Set up the nginx vhost
  nginx::resource::server { "${vhost} www.${vhost}":
    proxy                 => $backend,
    proxy_send_timeout    => '10s',
    proxy_read_timeout    => '10s',
    proxy_connect_timeout => '3s',
    server_cfg_append     => {
      'access_log' => "/var/log/nginx/${vhost}.log custom_cache_log",
    },
    location_cfg_append   => {
      'proxy_cache_valid'     => ['200 301 302 365d', '404 10d'],
      'add_header'            => 'X-Proxy-Cache $upstream_cache_status',
      'proxy_cache'           => 'backendcache',
      'proxy_ignore_headers'  => $ignoredheaders,
      'proxy_cache_use_stale' => 'error timeout invalid_header updating http_500 http_502 http_503 http_504',
    },
  }

  # Make sure we have logrotate rules so our logs don't grow infinitely
  logrotate::conf { '/etc/logrotate.conf':
    su => $logrotate_su,
  }

  logrotate::rule { 'nginx':
    ensure        => present,
    path          => '/var/log/nginx/*.log',
    rotate        => 3,
    maxsize       => $maxlogsize,
    missingok     => true,
    compress      => true,
    delaycompress => true,
    ifempty       => false,
    sharedscripts => true,
    create        => true,
    create_mode   => '0640',
    create_owner  => 'nginx',
    create_group  => 'adm',
    postrotate    => '[ ! -f /var/run/nginx.pid ] || kill -USR1 `cat /var/run/nginx.pid`',
  }
}
