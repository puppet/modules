# Class to manage DHCP services
#
class profile::dhcpserver (
  String $interface = 'eth0'
) {
  # Ensure the packet is installed
  package { 'isc-dhcp-server':
    ensure => installed,
  }

  # Make sure DHCP daemon is running
  service { 'isc-dhcp-server':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
    require    => Package['isc-dhcp-server'],
  }

  # Manage config
  file { '/etc/dhcp/dhcpd.conf':
    ensure  => file,
    content => template('profile/dhcpd.conf.erb'),
    notify  => Service['isc-dhcp-server'],
  }

  file { '/etc/default/isc-dhcp-server':
    ensure  => file,
    content => "INTERFACESv4=\"${interface}\"",
    notify  => Service['isc-dhcp-server'],
  }
}
