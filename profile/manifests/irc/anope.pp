# Class: profile::irc::anope
#
# * Add anope user to system
# * Deal with downloading a pre-compiled copy of anope service
# * Create and manage systemd service to run anope
# * Manage anope config file
#
class profile::irc::anope (
  String  $linkpass,
  Integer $seed,
  String  $user    = 'anope',
  String  $homedir = "/home/${user}",
) {
  include ::profile::base::packages
  include ::profile::borg::client
  include ::profile::domaincerts
  include ::site_postfix::localrelay
  include ::site_rundeck::client
  include ::site_ssh::keys_only
  include ::systemd

  # Create user/group/homedir
  user { $user:
    ensure     => present,
    comment    => 'Anope IRC services user',
    home       => $homedir,
    shell      => '/bin/bash',
    managehome => true,
  }

  # Download services build archive
  -> exec { 'download-anope':
    command     => "/usr/bin/wget -O ${user}.tar.gz https://coolfire.insomnia247.nl/irc/${user}.tar.gz",
    cwd         => $homedir,
    user        => $user,
    notify      => Exec['extract-anope'],
    refreshonly => true,
  }

  # Extract archive when refresh is triggered
  exec { 'extract-anope':
    command     => "/bin/tar -zxf ${user}.tar.gz",
    cwd         => $homedir,
    user        => $user,
    refreshonly => true,
  }

  # Flag file to trigger extracting
  file { "${homedir}/refresh":
    ensure => present,
    owner  => $user,
    group  => $user,
    mode   => '0640',
    notify => Exec['download-anope']
  }

  # Manage systemd service
  file { "/etc/systemd/system/${user}.service":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('profile/irc/anope.service.erb'),
  }

  -> service { $user:
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }

  # Manage configuration file
  file { "${homedir}/services/conf/services.conf":
    ensure  => file,
    owner   => $user,
    group   => $user,
    mode    => '0640',
    content => template('profile/irc/anope-services.conf.erb'),
    notify  => Service[$user],
  }

  # Some extra ordering constraints
  User[$user]
  -> File["/home/${user}/refresh"]

  User[$user]
  -> File["${homedir}/services/conf/services.conf"]
}
