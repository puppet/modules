# Unreal IRC hub node profile
#
# @param user   Username for user that will run the leaf ircd
# @param config Hash of unrealircd configuration
#
class profile::irc::hub (
  Hash $config = {},
) {
  # Install the base IRCd
  profile::irc::server { 'unreal-hub':
    config => $config,
    type   => 'hub',
  }

}
