# Unreal IRC base server profile
#
# @param user   username for the IRCd
# @param type   leaf or hub node
# @param config configuration hash
#
define profile::irc::server (
  Hash   $config,
  String $type = 'leaf',
  String $user = "unreal-${type}"
) {
  include ::profile::base::packages
  include ::profile::base::snmpclient
  include ::profile::base::timesyncd
  include ::profile::domaincerts
  include ::site_postfix::localrelay
  include ::site_rundeck::client
  include ::site_ssh::keys_only
  include ::systemd

  # Create user/group/homedir
  user { $user:
    ensure     => present,
    comment    => "Unreal user for ${type}",
    home       => "/home/${user}",
    shell      => '/bin/bash',
    managehome => true,
    groups     => ['ssl-cert']
  }

  # Download ircd build archive
  -> exec { "download-${type}":
    command     => "/usr/bin/wget -O ${user}.tar.gz https://coolfire.insomnia247.nl/irc/${user}.tar.gz",
    cwd         => "/home/${user}",
    user        => $user,
    notify      => Exec["extract-${type}"],
    refreshonly => true,
  }

  # Extract precompiled ircd when refresh is triggered
  exec { "extract-${type}":
    command     => "/bin/tar -zxf ${user}.tar.gz",
    cwd         => "/home/${user}",
    user        => $user,
    refreshonly => true,
  }

  # Flag file to trigger extracting a new IRCd
  file { "/home/${user}/refresh":
    ensure => present,
    owner  => $user,
    group  => $user,
    mode   => '0640',
    notify => Exec["download-${type}"]
  }

  # Deal with systemd services
  file { "/etc/systemd/system/${user}.service":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('profile/irc/unrealircd.service.erb'),
  }

  -> service { $user:
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }

  # Deal with TLS certificates
  exec { "${user}-reload-cert":
    command     => "/home/${user}/unrealircd/unrealircd reloadtls",
    subscribe   => Class['::profile::domaincerts'],
    refreshonly => true,
  }

  Class['profile::domaincerts']
  -> User[$user]
  -> File["/home/${user}/refresh"]

  file {
    default:
      ensure => file,
      owner  => $user,
      group  => $user,
      mode   => '0400',
      tag    => [ "${user}-certfiles" ];
    "/home/${user}/unrealircd/conf/tls/server.cert.pem":
      source => '/etc/ssl/certs/insomnia247-nl_bundle.crt';
    "/home/${user}/unrealircd/conf/tls/server.key.pem":
      source => '/etc/ssl/private/insomnia247-nl.pem';
  }

  Class['profile::domaincerts']
  -> File<| tag == "${user}-certfiles" |>

  # Deal with unrealircd configuration files
  file { "/home/${user}/unrealircd/conf/unrealircd.conf":
    ensure  => file,
    owner   => $user,
    group   => $user,
    mode    => '0600',
    notify  => Exec["${user}-rehash"],
    content => template('profile/irc/unrealircd.conf.erb'),
  }

  file {
    default:
      ensure => file,
      owner  => $user,
      group  => $user,
      mode   => '0640',
      notify => Exec["${user}-rehash"];
    "/home/${user}/unrealircd/conf/ircd.motd":
      source => 'puppet:///modules/profile/unrealircd/ircd.motd';
    "/home/${user}/unrealircd/conf/ircd.rules":
      source => 'puppet:///modules/profile/unrealircd/ircd.rules';
  }

  exec { "${user}-rehash":
    command     => "systemctl reload ${user}",
    path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    refreshonly => true,
  }
}
