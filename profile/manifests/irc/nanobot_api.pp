# Class: profile::irc::nanobot_api
#
# - Set up OS packages
# - Manage certificates
# - Manage apache vhost
# - Deploy nanobot-api git repository
# - Manage API configuration file
#
class profile::irc::nanobot_api (
  Hash   $apiconf,
  String $user    = 'nanoapi',
  String $homedir = "/home/${user}",
  String $docroot = "${homedir}/nanoapi",
  String $vhost   = "${user}.insomnia247.nl",
) {
  include ::profile::base::packages
  include ::profile::domaincerts
  include ::site_apache
  include ::site_rundeck::client
  include ::site_ssh::keys_only
  include ::systemd

  # Create user
  user { $user:
    ensure     => present,
    comment    => 'Nanobot web api user',
    home       => $homedir,
    managehome => true,
    shell      => '/bin/bash',
  }

  # Create apache vhost
  -> site_apache::vhost::system { $vhost:
    customdirectives => "
  PassengerAppRoot ${docroot}/
  RailsEnv         production
  PassengerUser    ${user}
  PassengerGroup   ${user}"
  }

  # Pull down repository
  vcsrepo { $docroot:
    ensure   => latest,
    provider => git,
    user     => $user,
    group    => $user,
    source   => 'https://git.insomnia247.nl/coolfire/nanoapi.git',
  }

  ~> exec { 'bundle_install':
    command     => '/usr/bin/bundle install --path vendor/bundle',
    cwd         => $docroot,
    user        => $user,
    refreshonly => true,
  }

  # Write configuration file for application
  file { "${docroot}/config.yaml":
    ensure  => file,
    owner   => $user,
    group   => $user,
    mode    => '0750',
    content => inline_template('<%= @apiconf.to_yaml %>'),
  }

  ~> exec { "${vhost} Touch restart":
    command     => "/usr/bin/touch ${docroot}/tmp/restart.txt",
    user        => $user,
    refreshonly => true,
  }

  # Create logrotate rule for application log
  logrotate::rule { 'production_log':
    ensure       => present,
    rotate       => 12,
    rotate_every => 'month',
    missingok    => true,
    compress     => true,
    ifempty      => false,
    path         => "${docroot}/logs/production.log",
    postrotate   => "/usr/bin/touch ${docroot}/tmp/restart.txt",
  }

  Class['Profile::Base::Packages']
  -> Site_apache::Vhost::System[$vhost]
  -> Vcsrepo[$docroot]
  -> File["${docroot}/config.yaml"]
  -> Logrotate::Rule['production_log']

  Vcsrepo[$docroot]
  ~> Exec["${vhost} Touch restart"]
}
