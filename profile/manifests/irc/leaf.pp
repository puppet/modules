# Unreal IRC leaf node profile
#
# @param user   Username for user that will run the leaf ircd
# @param config Hash of unrealircd configuration
#
class profile::irc::leaf (
  Hash $config = {},
) {
  # Install the base IRCd
  profile::irc::server { 'unreal-leaf':
    config => $config,
  }

}
