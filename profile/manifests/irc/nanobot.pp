# Class: profile::irc::nanobot
#
class profile::irc::nanobot (
  String $user    = 'nanobot',
  String $homedir = "/home/${user}",
  String $nanodir = "${homedir}/nanobot"
) {
  include ::profile::borg::client

  # Create user
  user { $user:
    ensure     => present,
    comment    => 'Nanobot user',
    home       => $homedir,
    managehome => true,
    shell      => '/bin/bash',
  }

  # Pull down repository
  -> vcsrepo { $nanodir:
    ensure   => latest,
    provider => git,
    user     => $user,
    group    => $user,
    source   => 'https://git.insomnia247.nl/coolfire/nanobot4.git',
  }

  ~> exec { 'nanobot_bundle_install':
    command     => '/usr/bin/bundle install --path vendor/bundle',
    cwd         => $nanodir,
    user        => $user,
    refreshonly => true,
  }
}
