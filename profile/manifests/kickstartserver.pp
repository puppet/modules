# Class to manage nginx and kickstart profiles
#
# Installs and configures nginx as well as deploying the
# kickstart profiles it serves
#
# @param path Location on the filesystem where the kickstart files are stored
# @param vhost nginx vhost domain where the kickstart files will be served
#
class profile::kickstartserver(
  String $path  = '/var/www/kickstart',
  String $vhost = 'kickstart.insomnia247.nl',
) {
  include nginx

  file { $path:
    ensure  => directory,
    recurse => true,
    before  => File["${path}/ubuntu_base.cfg"],
  }

  file { "${path}/ubuntu_base.cfg":
    ensure => present,
    source => 'puppet:///modules/profile/kickstarts/ubuntu_base.cfg',
    before => Class['nginx'],
  }

  file { "${path}/debian_lydia.cfg":
    ensure => present,
    source => 'puppet:///modules/profile/kickstarts/debian_lydia.cfg',
    before => Class['nginx'],
  }

  nginx::resource::server { $vhost:
    www_root => $path,
  }
}
