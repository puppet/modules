# Class to install misc packages from debian backports
#
# Package list is built by using a deep merge on 'profile::deb_backports'.
#
class profile::deb_backports {
  include apt
  include apt::backports

  $packages = lookup('profile::deb_backports', Array, 'unique', [])

  $packages.each |String $package| {
    package { $package:
      ensure          => 'installed',
      install_options => ['-t', ' bookworm-backports'],
      require         => Class['apt::backports']
    }
  }

  Class['apt::backports'] ~> Exec['apt_update']
}
