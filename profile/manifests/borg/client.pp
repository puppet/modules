# Class: profile::borgserver
#
class profile::borg::client(
  String  $ssh_key,
  String  $packagename = 'borgbackup',
  String  $borgserver  = 'borgserver@borg.pve.insomnia247.nl',
  Integer $borgport    = 22,
  Hash    $backups     = {},
  String  $mailto      = 'coolfire@insomnia247.nl',
) {
  include ::site_ssh::client

  # Install deps
  package { $packagename:
    ensure => installed,
  }

  file { '/root/.ssh/id_rsa_borg':
    ensure  => file,
    content => Sensitive($ssh_key),
    mode    => '0600',
  }

  file { '/root/borg':
    ensure => directory,
    mode   => '0600',
  }

  $backups.each |$backup_name, $backup_options| {
    # Borg init
    file { "/root/borg/${backup_name}_init.sh":
      ensure  => file,
      mode    => '0750',
      content => template('profile/borg/borg_client_init.sh.erb'),
      require => File['/root/borg'],
    }

    ~> exec { "${backup_name}_init":
      command     => "/root/borg/${backup_name}_init.sh",
      path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
      refreshonly => true,
      require     => File['/root/.ssh/id_rsa_borg'],
    }

    # Borg create
    file { "/root/borg/${backup_name}_create.sh":
      ensure  => file,
      mode    => '0750',
      content => template('profile/borg/borg_client_create.sh.erb'),
      require => File['/root/borg'],
    }

    cron { "${backup_name}_cron":
      command     => "/root/borg/${backup_name}_create.sh",
      environment => "MAILTO=\"${mailto}\"",
      special     => 'daily',
    }
  }
}
