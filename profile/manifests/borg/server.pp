# Class: profile::borgserver
#
class profile::borg::server {
  include ::site_ssh::keys_only

  user { 'borgserver':
    ensure  => present,
    comment => 'Borg Backup',
    home    => '/home/borgserver',
  }

  -> file { '/backups':
    ensure => directory,
    owner  => 'borgserver',
    group  => 'borgserver',
    mode   => '0660',
  }
}
