# Class: profile::lydia::outbound_mail_firewall
#
# Configure firewall rules to prevent abuse of outbound mail
#
class profile::lydia::outbound_mail_firewall (
  Array  $dports    = [25, 465, 587],
  String $localnet  = '10.0.1.0/24',
  String $local6net = 'fd5d:12c9:4201::/64',
) {
  include firewall

  firewall {
    default:
      chain => 'OUTPUT',
      jump  => 'accept',
      proto => 'tcp',
      dport => $dports;
    '010 Accept from self':
      destination => $localnet;
    '010 Accept from self v6':
      destination => $local6net,
      protocol    => 'IPv6';
    '011 Accept over loopback':
      outiface => 'lo';
    '011 Accept over loopback v6':
      outiface => 'lo',
      protocol => 'IPv6';
    '012 Accept for postfix':
      gid => 'postfix';
    '012 Accept for postfix v6':
      gid      => 'postfix',
      protocol => 'IPv6';
    # '013 Accept for power-users':
    #   gid => 'power-users';
    # '013 Accept for power-users v6':
    #   gid      => 'power-users',
    #   protocol => 'IPv6';
    '020 Drop for everything else':
      jump => 'drop';
    '020 Drop for everything else v6':
      jump     => 'drop',
      protocol => 'IPv6';
  }
}
