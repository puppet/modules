# Type to deal with user accounts and all their extras on Lydia
#
# @param properties
#
define profile::lydia::user (
  Hash $properties,
) {
  $username = $name

  # Home directory
  if 'removed' in $properties {
    $ensure      = 'absent'
    $ensure_home = 'absent'
    $ensure_link = 'absent'
    $ensure_file = 'absent'
    $ensure_cron = 'absent'
    $ensure_sql  = 'absent'
  } elsif 'archived' in $properties {
    $ensure      = 'present'
    $ensure_home = 'directory'
    $ensure_link = 'link'
    $ensure_file = 'file'
    $ensure_cron = 'absent'
    $ensure_sql  = 'absent'
  } else {
    $ensure      = 'present'
    $ensure_home = 'directory'
    $ensure_link = 'link'
    $ensure_file = 'file'
    $ensure_cron = 'file'
    $ensure_sql  = 'present'
  }

  unless ('archived' in $properties) and ($properties['archived'] == 'deep') {
    file {
      default:
        force => true,
        owner => $username,
        group => $username,
        mode  => '0750';
      "/home/${username}":
        ensure => $ensure_home;
      "/home/${username}/.bash_history":
        ensure => $ensure_file,
        mode   => '0600';
      "/coldstorage/${username}":
        ensure => $ensure_home;
      "/home/${username}/coldstorage":
        ensure => $ensure_link,
        target => "/coldstorage/${username}";
      "/home/${username}/.config":
        ensure => $ensure_home;
      "/home/${username}/.config/insomnia247":
        ensure => $ensure_home;
      "/var/mail/${username}":
        ensure => $ensure_file,
        group  => 'mail',
        mode   => '0600';
      "/var/spool/cron/crontabs/${username}":
        ensure => $ensure_cron,
        group  => 'crontab',
        mode   => '0600';
      "/var/lib/systemd/linger/${username}":
        ensure => $ensure_file,
        owner  => 'root',
        group  => 'root',
        mode   => '0644';
    }

    # Deploy API key
    if 'api_key' in $properties {
      file { "/home/${username}/.config/insomnia247/api_key":
        ensure  => $ensure_file,
        owner   => $username,
        group   => $username,
        mode    => '0400',
        require => File["/home/${username}/.config/insomnia247"],
        content => $properties['api_key'],
      }
    }

    # Set quotas
    $quota_options = 'quota' in $properties ? {
      true  => $properties['quota'],
      false => {},
    }

    ::site_quota::user { $username:
      removed => 'removed' in $properties,
      *       => $quota_options,
    }

    # Ensure the home files and directories are created in the right order
    File["/home/${username}"] -> File["/home/${username}/.bash_history"]
    File["/home/${username}"] -> ::Site_quota::User[$username]
    File["/home/${username}"]
    -> File["/coldstorage/${username}"]
    -> File["/home/${username}/coldstorage"]

    # Sudo config
    file { "/etc/sudoers.d/suwww_${username}":
      ensure  => $ensure,
      owner   => 'root',
      group   => 'root',
      mode    => '0660',
      content => "${username}\t\tALL = NOPASSWD: /bin/su ${username}-www -s /bin/bash\n",
    }

    # MOTD
    file { "/etc/news.d/${username}":
      ensure  => 'motd' in $properties ? { true => file, default => absent },
      owner   => $username,
      group   => $username,
      mode    => '0440',
      content => $properties['motd'],
    }

    unless 'removed' in $properties {
      # Copy skeleton files for new users
      exec { "exec_copy_skel_${username}":
        command => "/bin/cp -r /etc/skel/. /home/${username}/",
        creates => "/home/${username}/.bash_profile",
        user    => $username,
      }
    }

    # Apache vhosts
    file {
      default:
        ensure  => $ensure_home,
        force   => true,
        owner   => $username,
        group   => $username,
        mode    => '0750',
        require => File["/home/${username}"];
      "/home/${username}/http_logs":
      ;
      "/home/${username}/coldstorage/http_logs":
      ;
      "/home/${username}/public_html":
      ;
    }

    if 'apache_vhosts' in $properties {
      each($properties['apache_vhosts']) |String $domain, Hash $params| {
        ::site_apache::vhost::user { $domain:
          user    => $username,
          *       => $params,
          require => File["/home/${username}/http_logs"],
          notify  => Service['apache2'],
        }
      }
    }

    if 'apache_proxies' in $properties {
      each($properties['apache_proxies']) |String $domain, Hash $params| {
        ::site_apache::vhost::user_proxy { $domain:
          user    => $username,
          *       => $params,
          require => File["/home/${username}/http_logs"],
          notify  => Service['apache2'],
        }
      }
    }

    # Mysql databases
    if 'mysql_dbs' in $properties {
      each($properties['mysql_dbs']) |String $dbname, Hash $params| {
        $local_ensure_sql = 'removed' in $params ? {
          true  => absent,
          false => $ensure_sql,
        }
        $params_filtered = delete($params, 'removed')

        mysql::db { "${username}_${dbname}":
          ensure  => $local_ensure_sql,
          user    => "${username}_${dbname}",
          charset => 'utf8mb3',
          collate => 'utf8mb3_general_ci',
          *       => $params_filtered,
          grant   => ['ALL'],
        }
      }
    }
  }

  unless 'removed' in $properties {

    # Cgroups config
    $cgroups_defaults = {
      cpucap  => 0,
      memory  => '4g',
      enabled => true,
    }

    if 'cgroups' in $properties {
      $cgroups_settings = stdlib::merge(
        $cgroups_defaults,
        $properties['cgroups']
      )
    } else {
      $cgroups_settings = $cgroups_defaults
    }

    site_cgroups::user { $username:
      * => $cgroups_settings,
    }

    # Mail config
    concat::fragment { "postfix_virtual_${username}":
      target  => '/etc/postfix/virtual',
      content => template('site_postfix/virtual_user.erb'),
      order   => '05'
    }

    # Podman subuid/subgid mappings
    if 'uidmap_offset' in $properties {
      site_podman::user { $username:
        uidmap_offset => $properties['uidmap_offset'],
      }
    }
  }
}
