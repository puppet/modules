Puppet::Functions.create_function(:'generate_ptr') do
  dispatch :generate_ptr do
    param 'Hash', :zones
  end

  def generate_ptr(zones)
    require 'ipaddr'
    ptrzones = {}

    # Check which reverse zones we should generate PTR records for and copy any existing records
    zones.each do |zone, data|
      if zone.end_with? '.arpa'
        ptrzones[zone] = data
      end
    end

    # Loop over all forward zones
    zones.each do |zone, records|

      # Loop over all records in the zone
      records.each do |hostname, data|
        next if hostname == '*'

        # Loop over each hostname in the record
        data.each do |record|

          # Check if we need to consider making a PTR record based on the type
          next unless record['type'] == 'A' || record['type'] == 'AAAA'
      
          # Generate string for reverse DNS loopups
          rev = IPAddr.new(record['target']).reverse()

          # Generate PTR record targets
          if hostname == '@'
            h = "#{zone}."
          else
            h = "#{hostname}.#{zone}."
          end

          # Generate the PTR records for applicable zones
          ptrzones.each do |ptrzone, _|
            if rev.end_with? ptrzone
              ptrzones[ptrzone][rev.chomp(".#{ptrzone}")] = ['type' => 'PTR', 'target' => h]
            end
          end
        end
      end
    end

    zones.merge!(ptrzones)
    zones
  end
end
