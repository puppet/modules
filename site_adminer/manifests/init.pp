# Class: site_adminer
#
# Some custom configuration for adminer
#
class site_adminer {
  include profile::base::packages

  file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644';
    '/usr/share/adminer/.htaccess':
      source => 'puppet:///modules/site_adminer/htaccess';
    '/usr/share/adminer/adminer/adminer.css':
      source => 'puppet:///modules/site_adminer/adminer.css';
  }

  Class['Profile::Base::Packages'] -> Class['Site_adminer']
}
