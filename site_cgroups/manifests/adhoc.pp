# Class: site_cgroups::adhoc
#
# Create a few special cgroups that we can
# assign a one-off process to when needed
#
class site_cgroups::adhoc (
  Array[Integer] $cpucaps = [1, 5, 10, 25, 50],
) {
  concat::fragment { 'cgroups_adhoc_cpucaps':
    target  => '/etc/cgconfig.conf',
    content => template('site_cgroups/adhoc.erb'),
    order   => '04',
  }
}
