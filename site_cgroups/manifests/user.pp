# Define: site_cgroups::user
#
# Make cgroup group and rule for each user
# If a user is moved from having a cgroup config
# to not having one enabled, the cgroup needs to be
# removed by hand using the cgdelete command.
# Ex. cgdelete cpu,memory:/user_coolfire
#
define site_cgroups::user (
  String  $memory,
  Boolean $enabled,
  Integer $cpucap   = 0,
  String  $username = $name,
) {
  if $enabled {
    concat::fragment { "cgroups_group_${username}":
      target  => '/etc/cgconfig.conf',
      content => template('site_cgroups/usergroup.erb'),
      order   => '05',
    }

    concat::fragment { "cgroups_rule_${username}":
      target  => '/etc/cgrules.conf',
      content => template('site_cgroups/userrule.erb'),
      order   => '05',
    }
  }
}
