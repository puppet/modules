# Class: site_cgroups
#
# Main class for site_cgroups
#
# - Installs required packages
# - Manages cgmanager service
# - Gathers concat fragments for user config
#
class site_cgroups(
  Array $packages = [
    'cgroup-tools',
  ],
  String $cgrulesengd = '/usr/sbin/cgrulesengd --logfile=/var/log/cgrulesengd.log',
) {
  # Install packages
  package { $packages:
    ensure => installed,
  }

  # Gather config fragments
  concat { '/etc/cgconfig.conf':
    ensure => present,
    owner  => 'root',
    group  => 'root',
    mode   => '0640',
    notify => Exec['cgconfigparser'],
  }

  concat { '/etc/cgrules.conf':
    ensure => present,
    owner  => 'root',
    group  => 'root',
    mode   => '0640',
    notify => Exec['cgconfigparser'],
  }

  # Ensure config dir exists
  file { '/etc/cgconfig.d':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0750',
  }

  # Chain to apply new cgoups configurations
  exec { 'cgconfigparser':
    command     => '/usr/sbin/cgconfigparser -l /etc/cgconfig.conf',
    refreshonly => true,
    notify      => Exec['cgrulesengd'],
  }

  exec { 'cgrulesengd':
    command     => "pidof /usr/sbin/cgrulesengd && kill -USR2 $(pidof /usr/sbin/cgrulesengd) || ${cgrulesengd}",
    path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    refreshonly => true,
  }
}
