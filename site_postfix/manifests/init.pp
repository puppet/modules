# Class to manage postfix config
#
class site_postfix (
  String $certfile               = $::profile::domaincerts::certfile,
  String $keyfile                = $::profile::domaincerts::keyfile,
  String $cafile                 = $::profile::domaincerts::cafile,
  Array[String] $blocked_senders = [],
  Array[String] $header_checks   = [],
  String $forwarding_auth        = '',
) {
  include ::profile::base::packages
  include ::profile::domaincerts

  service { 'postfix':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
    require    => Class['profile::base::packages'],
  }

  # Write config files
  file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      notify => Service['postfix'];
    '/etc/postfix/main.cf':
      content => template('site_postfix/main.cf.erb');
    '/etc/postfix/master.cf':
      source => 'puppet:///modules/site_postfix/master.cf';
    '/etc/postfix/header_checks':
      content => template('site_postfix/header_checks.erb');
    '/etc/postfix/sasl/smtpd.conf':
      source => 'puppet:///modules/site_postfix/sasl/smtpd.conf';
    '/etc/postfix/login_map':
      content => template('site_postfix/login_map.erb');
    '/etc/postfix/sasl_passwd':
      mode    => '0600',
      notify  => Exec['postmap_forwarding_auth_build'],
      content => Sensitive($forwarding_auth);
    '/etc/postfix/sender_access':
      notify  => Exec['postmap_sender_access_build'],
      content => 'insomnia247.nl OK';
  }

  # Build virual.db
  concat { '/etc/postfix/virtual':
    ensure => present,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    notify => Exec['postmap_build'],
  }

  concat::fragment { 'postfix_virtual_header':
    target => '/etc/postfix/virtual',
    source => 'puppet:///modules/site_postfix/virtual_header',
    order  => '01'
  }

  exec { 'postmap_build':
    command     => '/usr/sbin/postmap -f /etc/postfix/virtual',
    refreshonly => true,
    notify      => Service['postfix'],
  }

  exec { 'postmap_forwarding_auth_build':
    command     => '/usr/sbin/postmap /etc/postfix/sasl_passwd',
    refreshonly => true,
    notify      => Service['postfix'],
  }

  exec { 'postmap_sender_access_build':
    command     => '/usr/sbin/postmap /etc/postfix/sender_access',
    refreshonly => true,
    notify      => Service['postfix'],
  }

  # Ensure postfix user is allowed to authenticate to sasld
  user { 'postfix':
    ensure => present,
  }

  User <| title == 'postfix' |> { groups +> 'sasl' }

  Class['profile::base::packages'] -> User['postfix']
  Class['::profile::domaincerts'] ~> Service['postfix']
}
