# Class: site_postfix::localrelay
#
class site_postfix::localrelay (
  String $certfile  = $::profile::domaincerts::certfile,
  String $keyfile   = $::profile::domaincerts::keyfile,
  String $cafile    = $::profile::domaincerts::cafile,
  String $relayhost = 'insomnia247.nl',
){
  include ::profile::base::packages

  service { 'postfix':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }

  file { '/etc/postfix/main.cf':
    ensure  => file,
    mode    => '0644',
    content => template('site_postfix/localrelay/main.cf.erb'),
    notify  => Service['postfix']
  }

  Class['profile::base::packages']
  -> Service['postfix']

  Class['profile::base::packages']
  -> File['/etc/postfix/main.cf']
}
