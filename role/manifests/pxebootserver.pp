# PXEboot tftp server role
#
class role::pxebootserver inherits role::base {
  include profile::base::securityupdates
  include profile::pxebootserver
  include profile::kickstartserver
  include profile::reposerver
}
