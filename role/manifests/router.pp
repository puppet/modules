# Role for router machines
#
class role::router inherits role::base {
  include profile::base::firewall
  include profile::dhcpserver
  include profile::dnsserver
  include profile::radvdserver
  include profile::router
  include site_ssh::keys_only
}
