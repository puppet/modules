# Class: role::cacheproxy
#
# Class to deploy nginx reverse proxy server with caching
#
class role::cacheproxy {
  include profile::base::auditbeat
  include profile::base::puppetagent
  include profile::base::securityupdates
  include profile::cacheproxy
  include site_ssh::keys_only
}
