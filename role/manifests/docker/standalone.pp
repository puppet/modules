# Role for docker standalone hosts
#
class role::docker::standalone (
  Boolean $enable_gitlab_runner = true,
) inherits role::base {
  if $enable_gitlab_runner {
    include gitlab_ci_runner
  }

  include profile::docker::standalone
}
