# Role for docker swarm hosts
#
class role::docker::swarm inherits role::base {
  include gitlab_ci_runner
  include profile::docker::swarm
}
