# Role for docker standalone hosts with puppet managed workloads
#
class role::docker::standalone_managed_workloads (
  Boolean $enable_gitlab_runner = true,
) inherits role::base {
  if $enable_gitlab_runner {
    include gitlab_ci_runner
  }

  include profile::docker::standalone
  include profile::docker::managed_workloads
}
