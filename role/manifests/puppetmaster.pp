# Puppet server role
#
class role::puppetmaster inherits role::base {
  include profile::puppetmaster
}
