# Class: role::manager_api
#
# Role for api.insomnia247.nl
#
class role::manager_api inherits role::base {
  include profile::manager_api
}
