# Role for VPN gateway
#
# This is the remote, internet facing machine
#
class role::vpn::gateway inherits role::base {
  include ::profile::base::dhparams
  include ::profile::base::firewall
  include ::profile::base::packages
  include ::profile::base::snmpclient
  include ::profile::openvpn::server
}
