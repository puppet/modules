# Role for VPN router
#
# This is the local machine which acts as the default gateway for
# servers using the VPN tunnel
#
class role::vpn::router inherits role::base {
  include ::profile::base::dhparams
  include ::profile::base::firewall
  include ::profile::base::packages
  include ::profile::router
  include ::profile::openvpn::client
  include ::profile::base::snmpclient
  include ::profile::base::tcp_bbr
}
