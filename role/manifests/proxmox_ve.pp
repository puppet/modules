# Role for Proxmox VE VM/CT hardware nodes
#
# Currently just used to include some base stuff
#
class role::proxmox_ve inherits role::base {
  include profile::domaincerts
  include profile::base::snmpclient
}
