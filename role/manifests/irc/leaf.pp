# Role for irc leaf nodes
#
class role::irc::leaf inherits role::base {
  include profile::irc::leaf
}
