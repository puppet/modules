# Class: role::irc::anope
#
# Install and configure anope IRC services
#
class role::irc::anope inherits role::base {
  include ::profile::irc::anope
}
