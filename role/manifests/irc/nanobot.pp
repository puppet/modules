# Class: role::irc::nanobot
#
# Install and configure nanobot
#
class role::irc::nanobot inherits role::base {
  include ::profile::irc::nanobot
}
