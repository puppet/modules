# Class: role::irc::nanobot_api
#
# Install and configure nginx and configure it for nanobot's api
#
class role::irc::nanobot_api inherits role::base {
  include ::profile::irc::nanobot_api
}
