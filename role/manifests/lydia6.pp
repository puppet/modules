# Role for Lydia
# This node is a special case since it is a multi-purpose server
# for many users.
#
class role::lydia6 inherits role::base {
  include profile::lydia
}
