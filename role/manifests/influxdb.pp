# Class: role::influxdb
#
class role::influxdb inherits role::base {
  include ::profile::influxdb
}
