# Class: role::borgserver
#
class role::borgserver inherits role::base{
  include profile::borg::server
}
