# Base roll that has stuff common to all nodes
#
class role::base {
  include profile::base::auditbeat
  include profile::base::grub
  include profile::base::mailname
  include profile::base::network_buffers
  include profile::base::powersave
  include profile::base::puppetagent
  include profile::base::securityupdates
  include profile::base::tcp_bbr
}
