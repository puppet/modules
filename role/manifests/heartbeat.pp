# Class: role::heartbeat
#
# Install and configure webserver for heartbeat/status page
#
class role::heartbeat inherits role::base {
  include ::profile::heartbeat
}
