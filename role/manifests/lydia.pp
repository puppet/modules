# Role for Lydia
# This node is a special case since it is a multi-purpose server
# for many users.
#
class role::lydia inherits role::base {
  include profile::base::packages
  include site_imagemagick
}
