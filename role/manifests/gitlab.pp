# Role for Gitlab server
#
# Currently just used to include some base stuff
#
class role::gitlab inherits role::base {
  include profile::domaincerts
  include profile::gitlab
}
