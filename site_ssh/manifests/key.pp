# Resource type for ssh server keys
#
# @param public Public portion of the host key
# @param private Private portion of the host key
#
define site_ssh::key (
  String $public,
  String $private,
) {
  # Write keypair
  file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0600',
      notify => Service['ssh'],
      tag    => ['files-sshd'];
    "/etc/ssh/ssh_host_${name}_key.pub":
      mode    => '0644',
      content => $public;
    "/etc/ssh/ssh_host_${name}_key":
      content => Sensitive($private);
  }
}
