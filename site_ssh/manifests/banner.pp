# Class: site_ssh::banner
#
#
class site_ssh::banner {
  file { '/etc/banner':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/site_ssh/banner',
  }
}
