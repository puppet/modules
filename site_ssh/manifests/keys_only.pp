# Class: site_ssh::keys_only
#
# Provides minimal sshd config which only allows login with ssh keys
#
class site_ssh::keys_only(
  String $service_name = 'sshd',
) {

$sshd_config = @(END)
ChallengeResponseAuthentication no
UsePAM yes
X11Forwarding no
PrintMotd no
AcceptEnv LANG LC_*
Subsystem sftp  /usr/lib/openssh/sftp-server
UseDNS no
PasswordAuthentication no
KexAlgorithms=-ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,diffie-hellman-group14-sha256
MACs=-hmac-sha1,hmac-sha1-etm@openssh.com,hmac-sha2-256,hmac-sha2-512,umac-128@openssh.com,umac-64-etm@openssh.com,umac-64@openssh.com
HostKeyAlgorithms=-ssh-rsa,ecdsa-sha2-nistp256
END

  file { '/etc/ssh/sshd_config':
    ensure  => file,
    mode    => '0600',
    content => $sshd_config,
  }

  ~> service { $service_name:
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }

  file { '/root/.ssh':
    ensure => directory,
    mode   => '0600',
  }

  -> file { '/root/.ssh/authorized_keys':
    ensure  => file,
    mode    => '0600',
    content => "ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdH\
A1MjEAAACFBADNaI0P6NAk1CVymgmVD77Q4EGGdHCTa7fnDA3h6G63tcX9PWLWGUnOQ2GwsqM5wMfj\
8/tfRYN6fLKyU+9ILl1eEwAUy5xCdbY28oUnly0u8/txUSp93tNnxZfRIp+PJOtUHzDQTqs+rb37Qx\
+S/YbEXMLR45Ot+/l7BbQfue1pre5SyQ== coolfire@insomnia247.nl"
  }
}
