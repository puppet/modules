# Class to configure ssh server
#
# @param hostkeys hash of public and private keys for the ssh server to use
# @param packages List of packages to install
#
class site_ssh (
  Hash  $hostkeys,
  Array $packages = [
    'libpam-google-authenticator',
    'openssh-client',
    'openssh-server',
    'openssh-sftp-server',
    'sshpass'
  ],
) {
  include ::site_ssh::banner
  include ::site_ssh::pam

  # Install packages
  package { $packages:
    ensure => installed,
    tag    => ['pkgs-sshd'],
  }

  # Write configuration files
  file { '/etc/ssh/sshd_config':
    ensure => file,
    mode   => '0600',
    source => 'puppet:///modules/site_ssh/sshd_config',
    tag    => ['files-sshd'],
    notify => Service['ssh'],
  }

  # Write keys
  create_resources('::site_ssh::key', $hostkeys)

  # Service definition
  service { 'ssh':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }

  # Resource ordering
  Package<| tag == 'pkgs-sshd' |>
  -> File<| tag == 'files-sshd' |>
  -> Service['ssh']
}
