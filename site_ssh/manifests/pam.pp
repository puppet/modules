# Class to handle pam config for ssh login
#
class site_ssh::pam {
  file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      tag    => ['files-sshd'],
      notify => Service['ssh'];
    '/etc/pam.d/common-account':
      source => 'puppet:///modules/site_ssh/pam/common-account';
    '/etc/pam.d/common-auth':
      source => 'puppet:///modules/site_ssh/pam/common-auth';
    '/etc/pam.d/common-password':
      source => 'puppet:///modules/site_ssh/pam/common-password';
    '/etc/pam.d/common-session':
      source => 'puppet:///modules/site_ssh/pam/common-session';
    '/etc/pam.d/common-session-noninteractive':
      source => 'puppet:///modules/site_ssh/pam/common-session-noninteractive';
    '/etc/pam.d/sshd':
      source => 'puppet:///modules/site_ssh/pam/sshd';
    '/etc/pam.d/login':
      source => 'puppet:///modules/site_ssh/pam/login';
  }
}
