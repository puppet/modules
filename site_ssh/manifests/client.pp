# Class: site_ssh::client
#
#
class site_ssh::client {
  file { '/etc/ssh/ssh_config':
    ensure => file,
    mode   => '0644',
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/site_ssh/ssh_config',
  }
}
