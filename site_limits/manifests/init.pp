# Class: site_limits
#
#
class site_limits {
  file { '/etc/security/limits.conf':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/site_limits/limits.conf',
  }
}
