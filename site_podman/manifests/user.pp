# Define: site_podman::user
#
# Manage configuration of subuid and subgid
#
define site_podman::user (
  Integer $uidmap_offset,
  String  $username    = $name,
  Integer $uidmap_base = 100000000,
) {
  concat::fragment { "podman_subuid_${username}":
    target  => '/etc/subuid',
    content => template('site_podman/usermap.erb'),
  }
  concat::fragment { "podman_subgid_${username}":
    target  => '/etc/subgid',
    content => template('site_podman/usermap.erb'),
  }
}
