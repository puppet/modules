# Class: site_podman
# Install and configure podman
#
class site_podman(
  Array[String] $registries = ['docker.io'],
) {
  package { 'podman':
    ensure => installed,
  }

  -> file {
    default:
      owner => 'root',
      group => 'root';
    '/etc/containers/registries.conf':
      ensure  => file,
      content => template('site_podman/registries.conf.erb');
    '/etc/containers/storage.conf':
      ensure  => file,
      content => template('site_podman/storage.conf.erb');
    '/usr/local/bin/docker':
      ensure => link,
      target => '/usr/bin/podman';
  }

  concat { '/etc/subuid':
    ensure => present,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }

  concat { '/etc/subgid':
    ensure => present,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }
}
