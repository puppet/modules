# Defined type for a user quota
#
# The quotas are inplemented on groups and under the
# assumption that each user has a primary group with
# the same name as the user. If you need manage by user
# behaviour, change the -g to -u in the commands.
#
# The condition where a user has no quota is not handeled
# very nicely as we basically check for the result of a
# miss-parsed output of Exec["quotareport"] that happens
# if no quota is set on the server.
#
define site_quota::user (
  String  $username = $name,
  String  $path     = '/',
  String  $homedir  = "/home/${username}",
  String  $soft     = '10000M',
  String  $hard     = '10250M',
  Boolean $hasquota = true,
  Boolean $removed  = false,
) {
  include ::site_quota

  $quotafile = "${homedir}/.config/insomnia247/diskquotas"

  if !$removed {
    # Trigger file to see if quota needs updating
    $expected_quota = $hasquota ? {
      true  => "${soft} ${hard}\n",
      false => "for group\n",
    }

    file { "fakequotareport_${username}":
      ensure  => file,
      path    => $quotafile,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => $expected_quota,
      notify  => Exec["setquota_${username}"]
    }

    # Command to set the user quota
    exec { "setquota_${username}":
      command     => "quotatool -g ${username} -b -q '${soft}' -l '${hard}' ${path}",
      path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
      refreshonly => true,
      notify      => Exec["quotareport_${username}"],
    }

    # Command to create .user_quotas file from actual quota command output
    exec { "quotareport_${username}":
      command     => "quota -s -g ${username} | tail -n1 | awk '{print \$3,\$4}' > ${quotafile}",
      path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
      refreshonly => true,
    }
  }
}
