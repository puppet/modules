# Main class for user quotas
#
# The drive/partition mount options to enable quotas are expected to be
# provided by the kickstart script during the partitioning stage.
#
class site_quota(
  String $base = '/',
  Array  $packages = [
    'quota',
    'quotatool'
  ],
) {
  # Install some required packages
  package { $packages:
    ensure => installed,
  }

  # Create quota files if needed
  exec { 'quotas_init':
    command => 'quotacheck -vagumc -F vfsv0',
    path    => '/bin:/sbin:/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    creates => "${base}aquota.group",
    notify  => Exec['quotas_enable'],
  }

  # Ensure quotas are enabled
  exec { 'quotas_enable':
    command     => "quotaon ${base}",
    path        => '/bin:/sbin:/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    refreshonly => true,
  }
}
