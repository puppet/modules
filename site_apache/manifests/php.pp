# Class: site_apache::php
# Set some custom php config
#
class site_apache::php {
  file { '/etc/php/8.2/apache2/conf.d/30-apache-overrides.ini':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/site_apache/php.ini',
  }
}
