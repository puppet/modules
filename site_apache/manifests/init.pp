# Class to manage apache config
#
class site_apache (
  Array $modules = [
    'brotli.load',
    'bw.load',
    'cgi.load',
    'expires.load',
    'headers.load',
    'passenger.conf',
    'passenger.load',
    'php8.2.conf',
    'php8.2.load',
    'proxy.conf',
    'proxy.load',
    'proxy_connect.load',
    'proxy_html.conf',
    'proxy_html.load',
    'proxy_http.load',
    'proxy_wstunnel.load',
    'rewrite.load',
    'socache_shmcb.load',
    'ssl.load',
    'xml2enc.load'
  ],
) {
  file { '/var/www/vhosts':
    ensure => directory,
    owner  => 'www-data',
    group  => 'www-data',
    mode   => '0640',
  }

  service { 'apache2':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    restart    => '/usr/sbin/apachectl graceful',
    hasstatus  => true,
  }

  # Enable apache modules
  each($modules) |String $module| {
    file { "/etc/apache2/mods-enabled/${module}":
      ensure => 'link',
      target => "/etc/apache2/mods-available/${module}",
      tag    => ['files-apache2-modules'],
    }
  }

  # Manage module config
  file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644';
    '/etc/apache2/mods-available/qos.conf':
      source => 'puppet:///modules/site_apache/qos.conf';
    '/etc/apache2/mods-available/ssl.conf':
      source => 'puppet:///modules/site_apache/ssl.conf';
    '/etc/apache2/conf-available/security.conf':
      source => 'puppet:///modules/site_apache/security.conf';
  }
  -> file {
    default:
      ensure => link,
      tag    => ['files-apache2-modules'];
    '/etc/apache2/mods-enabled/qos.conf':
      target => '/etc/apache2/mods-available/qos.conf';
    '/etc/apache2/mods-enabled/ssl.conf':
      target => '/etc/apache2/mods-available/ssl.conf';
    '/etc/apache2/conf-enabled/security.conf':
      target => '/etc/apache2/conf-available/security.conf';
  }
  -> file{
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      tag    => ['files-apache2-modules'];
    '/etc/apache2/ssl-medium.conf':
      source => 'puppet:///modules/site_apache/ssl-medium.conf';
    '/etc/apache2/ssl-strong.conf':
      source => 'puppet:///modules/site_apache/ssl-strong.conf';
  }

  # Remove link to obsolete default
  -> file { '/etc/apache2/sites-enabled/000-default.conf':
    ensure => absent,
  }

  # Remove undesirable config for global cgi-bin
  file { '/etc/apache2/conf-enabled/serve-cgi-bin.conf':
    ensure => absent,
  }

  # Remove system logrotate rules and manage our own
  file { '/etc/logrotate.d/apache2':
    ensure => absent,
  }

  # Ensure ordering
  File['/var/www/vhosts']
  -> File<| tag == 'files-apache2-modules' |>
  ~> Service['apache2']

  Class['::profile::base::packages']
  -> Class['::site_apache']
  Class['::profile::base::dhparams'] ~> Service['apache2']
  Class['::profile::domaincerts'] ~> Service['apache2']
}
