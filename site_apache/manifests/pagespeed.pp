# Class to install mod_pagespeed
#
# Only works on debian/apt base systems
#
class site_apache::pagespeed (
  String $source_location = 'http://dl.google.com/linux/mod-pagespeed/deb/',
  String $source_release  = 'stable',
  String $source_repos    = 'main',
) {
  include apt

  apt::key { 'Google, Inc. Linux Package Signing Key <linux-packages-keymaster@google.com>':
    id => '4CCA1EAF950CEE4AB83976DCA040830F7FAC5991',
  }

  -> apt::key { 'Google, Inc. (Linux Package Signing Authority) <linux-packages-keymaster@google.com>':
    id => 'EB4C1BFD4F042F6DDDCCEC917721F63BD38B4796',
  }

  -> apt::source { 'google-pagespeed':
    comment  => 'This is the google repo for mod_pagespeed',
    location => $source_location,
    release  => $source_release,
    repos    => $source_repos,
    include  => {
      'src' => false,
      'deb' => true,
    },
    notify   => Exec['apt_update'],
  }

  -> package { 'mod-pagespeed-stable':
    ensure => present,
  }

  -> file { '/etc/apache2/mods-available/pagespeed.conf':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/site_apache/pagespeed.conf',
  }

  -> file { '/etc/apache2/mods-enabled/pagespeed.conf':
    ensure => link,
    target => '/etc/apache2/mods-available/pagespeed.conf',
  }

  Package['mod-pagespeed-stable'] ~> Service['apache2']
}
