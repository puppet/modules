# Type to add apache vhosts
#
# TODO: document parameters
#
define site_apache::vhost::system_proxy (
  String  $location,
  String  $user             = 'www-data',
  String  $domain           = $name,
  Array   $aliases          = [],
  String  $customdirectives = '',
  Boolean $systemvhost      = true,
  String  $priority         = '10',
  Boolean $removed          = false,
  Boolean $disabled         = false,
  Boolean $strongtls        = true,
  Boolean $hsts             = true,
  String  $websockets       = '',
  Boolean $gzip             = false,
  String  $certfile         = $::profile::domaincerts::certfile,
  String  $keyfile          = $::profile::domaincerts::keyfile,
  String  $cafile           = $::profile::domaincerts::cafile,
) {

  if $removed {
    $ensure_default = 'absent'
    $ensure_docroot = 'absent'
    $ensure_config  = 'absent'
    $ensure_link    = 'absent'
  } elsif $disabled {
    $ensure_default = 'absent'
    $ensure_docroot = 'directory'
    $ensure_config  = 'absent'
    $ensure_link    = 'absent'
  } else {
    $ensure_default = 'present'
    $ensure_docroot = 'directory'
    $ensure_config  = 'file'
    $ensure_link    = 'link'
  }

  # write normal vhost config
  file { "/etc/apache2/sites-available/proxy_${domain}.conf":
    ensure  => $ensure_config,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('site_apache/vhost/proxy.erb'),
    notify  => Service['apache2'],
  }

  -> file { "/etc/apache2/sites-enabled/${priority}-proxy_${domain}.conf":
    ensure => $ensure_link,
    target => "/etc/apache2/sites-available/proxy_${domain}.conf",
  }

  # Logrotate setup
  $logrotate_defaults = {
    ensure        => $ensure_default,
    rotate        => 3,
    rotate_every  => 'month',
    missingok     => true,
    compress      => true,
    ifempty       => false,
    sharedscripts => true,
    create        => true,
    create_mode   => '0640',
    create_owner  => $user,
    create_group  => $user,
    postrotate    => 'echo "$1" >> /var/log/apache2/rotate.log'
  }

  logrotate::rule {
    default:
      * => $logrotate_defaults;
    "proxy_${domain}_error.log":
      path => "/var/log/apache2/${domain}_proxy_error.log";
    "proxy_${domain}_access.log":
      path => "/var/log/apache2/${domain}_proxy_access.log";
  }

  # Write ssl vhost config
  file { "/etc/apache2/sites-available/proxy_${domain}_ssl.conf":
    ensure  => $ensure_config,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('site_apache/vhost/proxy_ssl.erb'),
    notify  => Service['apache2'],
  }

  -> file { "/etc/apache2/sites-enabled/${priority}-proxy_${domain}_ssl.conf":
    ensure => $ensure_link,
    target => "/etc/apache2/sites-available/proxy_${domain}_ssl.conf",
  }
}
