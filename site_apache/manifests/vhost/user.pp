# Type to add apache vhosts
#
# TODO: document parameters
#
define site_apache::vhost::user (
  String  $user,
  String  $domain           = regsubst($name, '_', '-', 'G'),
  String  $documentroot     = "/home/${user}/public_html/${domain}",
  Array   $aliases          = [],
  String  $customdirectives = '',
  Integer $bwlimit          = 2000000,
  Boolean $customdomain     = false,
  String  $customcertdir    = "/home/${user}/tls/",
  Boolean $usewww           = true,
  Boolean $systemvhost      = false,
  Boolean $requirelimits    = true,
  Boolean $removed          = false,
  Boolean $disabled         = false,
  Boolean $strongtls        = false,
  Boolean $hsts             = false,
  Boolean $gzip             = false,
  Boolean $archived         = false,
  String $certfile          = $::profile::domaincerts::certfile,
  String $keyfile           = $::profile::domaincerts::keyfile,
  String $cafile            = $::profile::domaincerts::cafile,
) {
  if $removed or $archived {
    $ensure_default = 'absent'
    $ensure_docroot = 'absent'
    $ensure_config  = 'absent'
    $ensure_link    = 'absent'
  } elsif $disabled {
    $ensure_default = 'absent'
    $ensure_docroot = 'directory'
    $ensure_config  = 'absent'
    $ensure_link    = 'absent'
  } else {
    $ensure_default = 'present'
    $ensure_docroot = 'directory'
    $ensure_config  = 'file'
    $ensure_link    = 'link'
  }

  # Mange documentroot.
  file { $documentroot:
    ensure => $ensure_docroot,
    force  => true,
    owner  => $user,
    group  => $user,
    mode   => '0770',
  }

  # write normal vhost config
  file { "/etc/apache2/sites-available/user_${user}_${domain}.conf":
    ensure  => $ensure_config,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('site_apache/vhost/local.erb'),
    require => File[$documentroot],
  }

  -> file { "/etc/apache2/sites-enabled/20-user_${user}_${domain}.conf":
    ensure => $ensure_link,
    target => "/etc/apache2/sites-available/user_${user}_${domain}.conf",
  }

  # Extra work for lets-encrypt vhosts
  if $customdomain {
    $domains = $usewww ? {
      false   => [$domain],
      default => [$domain, "www.${domain}"],
    }

    letsencrypt::certonly { "LE_${user}_${domain}":
      ensure               => $ensure_default,
      manage_cron          => true,
      domains              => $domains,
      plugin               => 'apache',
      cron_output          => suppress,
      deploy_hook_commands => [
        "mkdir -p ${customcertdir}",
        "cp -Lr \$RENEWED_LINEAGE ${customcertdir}",
        "chown -R ${user}:${user} ${customcertdir}"
      ],
      before               => File["/etc/apache2/sites-available/user_${user}_${domain}_ssl.conf"],
    }
  }

  # Write ssl vhost config
  file { "/etc/apache2/sites-available/user_${user}_${domain}_ssl.conf":
    ensure  => $ensure_config,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('site_apache/vhost/local_ssl.erb'),
    require => File[$documentroot],
    notify  => Service['apache2'],
  }

  -> file { "/etc/apache2/sites-enabled/20-user_${user}_${domain}_ssl.conf":
    ensure => $ensure_link,
    target => "/etc/apache2/sites-available/user_${user}_${domain}_ssl.conf",
  }

  # Custom logrotate config
  file { "/etc/logrotate.d/user_${user}_${domain}.log":
    ensure  => $ensure_config,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('site_apache/logrotate/userlog.erb'),
  }
}
