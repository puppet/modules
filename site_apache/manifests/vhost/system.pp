# Type to add apache vhosts
#
# TODO: document parameters
#
define site_apache::vhost::system (
  String  $user             = 'www-data',
  String  $domain           = $name,
  Array   $aliases          = [],
  String  $documentroot     = "/var/www/vhosts/${name}",
  String  $customdirectives = '',
  Boolean $systemvhost      = true,
  Boolean $requirelimits    = true,
  String  $priority         = '10',
  Boolean $removed          = false,
  Boolean $disabled         = false,
  Boolean $strongtls        = true,
  Boolean $hsts             = true,
  Boolean $gzip             = false,
  String  $certfile         = $::profile::domaincerts::certfile,
  String  $keyfile          = $::profile::domaincerts::keyfile,
  String  $cafile           = $::profile::domaincerts::cafile,
) {


  if $removed {
    $ensure_default = 'absent'
    $ensure_docroot = 'absent'
    $ensure_config  = 'absent'
    $ensure_link    = 'absent'
  } elsif $disabled {
    $ensure_default = 'absent'
    $ensure_docroot = 'directory'
    $ensure_config  = 'absent'
    $ensure_link    = 'absent'
  } else {
    $ensure_default = 'present'
    $ensure_docroot = 'directory'
    $ensure_config  = 'file'
    $ensure_link    = 'link'
  }

  # Mange documentroot.
  file { $documentroot:
    ensure => $ensure_docroot,
    force  => true,
    owner  => $user,
    group  => $user,
    mode   => '0750',
  }

  # write normal vhost config
  file { "/etc/apache2/sites-available/system_${domain}.conf":
    ensure  => $ensure_config,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('site_apache/vhost/local.erb'),
    require => File[$documentroot],
    notify  => Service['apache2'],
  }

  -> file { "/etc/apache2/sites-enabled/${priority}-system_${domain}.conf":
    ensure => $ensure_link,
    target => "/etc/apache2/sites-available/system_${domain}.conf",
  }

  # Logrotate setup
  $logrotate_defaults = {
    ensure        => $ensure_default,
    rotate        => 3,
    rotate_every  => 'month',
    missingok     => true,
    compress      => true,
    ifempty       => false,
    sharedscripts => true,
    create        => true,
    create_mode   => '0640',
    create_owner  => $user,
    create_group  => $user,
    postrotate    => 'echo "$1" >> /var/log/apache2/rotate.log'
  }

  logrotate::rule {
    default:
      * => $logrotate_defaults;
    "system_${domain}_error.log":
      path => "/var/log/apache2/${domain}_error.log";
    "system_${domain}_access.log":
      path => "/var/log/apache2/${domain}_access.log";
  }

  # Write ssl vhost config
  file { "/etc/apache2/sites-available/system_${domain}_ssl.conf":
    ensure  => $ensure_config,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('site_apache/vhost/local_ssl.erb'),
    require => File[$documentroot],
    notify  => Service['apache2'],
  }

  -> file { "/etc/apache2/sites-enabled/${priority}-system_${domain}_ssl.conf":
    ensure => $ensure_link,
    target => "/etc/apache2/sites-available/system_${domain}_ssl.conf",
  }

  logrotate::rule {
    default:
      * => $logrotate_defaults;
    "system_${domain}_ssl_error.log":
      path => "/var/log/apache2/${domain}_ssl_error.log";
    "system_${domain}_ssl_access.log":
      path => "/var/log/apache2/${domain}_ssl_access.log";
  }
}
