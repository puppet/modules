# Type to add apache vhosts
#
# TODO: document parameters
#
define site_apache::vhost::user_proxy (
  String  $user,
  String  $location,
  String  $domain           = regsubst($name, '_', '-', 'G'),
  Array   $aliases          = [],
  Integer $bwlimit          = 2000000,
  String  $customdirectives = '',
  Boolean $customdomain     = false,
  String  $customcertdir    = "/home/${user}/tls/",
  Boolean $usewww           = true,
  Boolean $systemvhost      = false,
  Boolean $removed          = false,
  Boolean $disabled         = false,
  Boolean $strongtls        = false,
  Boolean $hsts             = false,
  Boolean $gzip             = false,
  String  $websockets       = '',
  Boolean $archived         = false,
  String  $certfile         = $::profile::domaincerts::certfile,
  String  $keyfile          = $::profile::domaincerts::keyfile,
  String  $cafile           = $::profile::domaincerts::cafile,
) {
  if $removed or $archived {
    $ensure_default = 'absent'
    $ensure_docroot = 'absent'
    $ensure_config  = 'absent'
    $ensure_link    = 'absent'
  } elsif $disabled {
    $ensure_default = 'absent'
    $ensure_docroot = 'directory'
    $ensure_config  = 'absent'
    $ensure_link    = 'absent'
  } else {
    $ensure_default = 'present'
    $ensure_docroot = 'directory'
    $ensure_config  = 'file'
    $ensure_link    = 'link'
  }

  # write normal vhost config
  file { "/etc/apache2/sites-available/user_proxy_${user}_${domain}.conf":
    ensure  => $ensure_config,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('site_apache/vhost/proxy.erb'),
    notify  => Service['apache2'],
  }

  -> file { "/etc/apache2/sites-enabled/20-user_proxy_${user}_${domain}.conf":
    ensure => $ensure_link,
    target => "/etc/apache2/sites-available/user_proxy_${user}_${domain}.conf",
  }

  # Logrotate setup
  $logrotate_defaults = {
    ensure        => $ensure_default,
    rotate        => 3,
    rotate_every  => 'month',
    missingok     => true,
    compress      => true,
    ifempty       => false,
    sharedscripts => true,
    create        => true,
    create_mode   => '0640',
    create_owner  => $user,
    create_group  => $user,
    copytruncate  => true,
    olddir        => "/coldstorage/${user}/http_logs/",
    postrotate    => '/etc/init.d/apache2 reload > /dev/null'
  }

  logrotate::rule {
    default:
      * => $logrotate_defaults;
    "user_proxy_${user}_${domain}_error.log":
      path => "/home/${user}/http_logs/${domain}_proxy_error.log";
    "user_proxy_${user}_${domain}_access.log":
      path => "/home/${user}/http_logs/${domain}_proxy_access.log";
  }

  # Extra work for lets-encrypt vhosts
  if $customdomain {
    $domains = $usewww ? {
      false   => [$domain],
      default => [$domain, "www.${domain}"],
    }

    letsencrypt::certonly { "LE_${user}_${domain}":
      ensure               => $ensure_default,
      manage_cron          => true,
      domains              => $domains,
      plugin               => 'apache',
      cron_output          => suppress,
      deploy_hook_commands => [
        "mkdir -p ${customcertdir}",
        "cp -Lr \$RENEWED_LINEAGE ${customcertdir}",
        "chown -R ${user}:${user} ${customcertdir}"
      ],
      before               => File["/etc/apache2/sites-available/user_proxy_${user}_${domain}_ssl.conf"],
    }
  }

  # Write ssl vhost config
  file { "/etc/apache2/sites-available/user_proxy_${user}_${domain}_ssl.conf":
    ensure  => $ensure_config,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('site_apache/vhost/proxy_ssl.erb'),
    notify  => Service['apache2'],
  }

  -> file { "/etc/apache2/sites-enabled/20-user_proxy_${user}_${domain}_ssl.conf":
    ensure => $ensure_link,
    target => "/etc/apache2/sites-available/user_proxy_${user}_${domain}_ssl.conf",
  }
}
