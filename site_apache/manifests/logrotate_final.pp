# Class: site_apache::logrotate_final
#
#
class site_apache::logrotate_final {
  logrotate::rule { 'zzz_apache_logrotate_final':
    ensure        => 'present',
    path          => '/var/log/apache2/rotate.log',
    rotate        => 3,
    rotate_every  => 'day',
    missingok     => true,
    compress      => true,
    delaycompress => true,
    ifempty       => false,
    sharedscripts => true,
    create        => true,
    create_mode   => '0640',
    create_owner  => 'root',
    create_group  => 'root',
    postrotate    => '/etc/init.d/apache2 reload > /dev/null',
  }
}
