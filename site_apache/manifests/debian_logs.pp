# Class: site_apache::debian_logs
#
# Manage the default debian apache logrotate rules
#
class site_apache::debian_logs {
  $logrotate_defaults = {
    ensure        => 'present',
    rotate        => 3,
    rotate_every  => 'month',
    missingok     => true,
    compress      => true,
    delaycompress => true,
    ifempty       => false,
    sharedscripts => true,
    create        => true,
    create_mode   => '0640',
    create_owner  => 'root',
    create_group  => 'root',
    postrotate    => 'echo "$1" >> /var/log/apache2/rotate.log'
  }

  logrotate::rule {
    default:
      * => $logrotate_defaults;
    'debian_access_log':
      path => '/var/log/apache2/access.log';
    'debian_other_vhosts_access_log':
      path => '/var/log/apache2/other_vhosts_access.log';
    'debian_error_log':
      path => '/var/log/apache2/error.log';
  }
}
