# Class: site_shellinabox
#
# Install shellinabox and configure apache vhost for it
#
class site_shellinabox (
  String $domain,
  String $confdir = '/etc/shellinabox/options-enabled',
) {
  include ::site_apache

  package { 'shellinabox':
    ensure => installed,
  }

  file {
    default:
      owner => 'root',
      group => 'root';
    "${confdir}/00_White On Black.css":
      ensure => absent;
    "${confdir}/00+Black on White.css":
      ensure => absent;
    "${confdir}/00+White On Black.css":
      ensure => link,
      target => '../options-available/00_White On Black.css';
    "${confdir}/00_Black on White.css":
      ensure => link,
      target => '../options-available/00+Black on White.css';
  }

  ~> service { 'shellinabox':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }

  site_apache::vhost::system_proxy { $domain:
    strongtls => true,
    location  => 'https://localhost:4200/'
  }
}
