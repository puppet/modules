# Class: site_kernelcare::apt
#
#
class site_kernelcare::apt {
  file { '/tmp/kernelcare-latest-10.deb':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0750',
    source => 'puppet:///modules/site_kernelcare/kernelcare-latest-10.deb'
  }

  ~> package { 'kernelcare':
    ensure   => installed,
    provider => 'dpkg',
    source   => '/tmp/kernelcare-latest-10.deb',
  }
}
