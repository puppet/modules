# Class: site_kernelcare
#
#
class site_kernelcare (
  Variant[String, Boolean] $key = false,
) {
  include site_kernelcare::apt

  if $key {
    exec { 'kernelcare_register':
      command => "kcarectl --register ${$key} && touch /etc/sysconfig/kcare/registered",
      path    => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
      creates => '/etc/sysconfig/kcare/registered',
    }

    Package['kernelcare'] -> Exec['kernelcare_register']
  }
}
