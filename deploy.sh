#!/usr/bin/bash

eval $(ssh-agent -s)
ssh-add <(echo "$SSH_PRIVATE_KEY")
mkdir -p ~/.ssh
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

for c in {1..4}
do
  ssh puppet.insomnia247.nl "cd /etc/puppetlabs/code/environments/production/modules && git pull" && break || sleep 3 && echo "retry"
done
