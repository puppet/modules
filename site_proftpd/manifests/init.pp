# Class to manage proftpd setup
#
class site_proftpd (
  String $masqueradeaddress,
  String $tlscert,
  String $tlskey,
  String $tlsca,
  String $servername     = $facts['networking']['hostname'],
  String $accessdenymsg  = 'Access denied.',
  String $modulepath     = '/usr/lib/proftpd',
  String $tlsprotocol    = 'TLSv1.2',
  String $tlsciphersuite = 'AES128+EECDH:AES128+EDH',
) {
  include ::profile::base::packages
  include ::profile::domaincerts

  service { 'proftpd':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
    require    => Class['::profile::base::packages'],
  }

  file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      notify => Service['proftpd'];
    '/etc/proftpd/proftpd.conf':
      content => template('site_proftpd/proftpd.conf.erb');
    '/etc/proftpd/tls.conf':
      content => template('site_proftpd/tls.conf.erb');
    '/etc/proftpd/modules.conf':
      content => template('site_proftpd/modules.conf.erb');
  }

  Class['::profile::base::dhparams'] ~> Service['proftpd']
  Class['::profile::domaincerts'] ~> Service['proftpd']
}
