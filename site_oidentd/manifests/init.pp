# Class: site_oidentd
#
# Class to install and configure oidentd
#
class site_oidentd (
  Array $packages = [
    'oidentd',
  ],
) {
  # Install packages
  package { $packages:
    ensure => installed,
  }

  # Create config file
  -> file { '/etc/oidentd.conf':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/site_oidentd/oidentd.conf';
  }

  # Manage service
  ~> service { 'oidentd':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }
}
