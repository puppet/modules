# Class: site_imagemagick
#
# Helper class to ensure imagemagick is installed with a safe policy file
#
class site_imagemagick (
    String $policyfile = '/etc/ImageMagick-6/policy.xml',
) {
  package { 'imagemagick':
    ensure => installed,
  }

  -> file { $policyfile:
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/site_imagemagick/policy.xml',
  }
}
