# Class: site_tor
#
# Install and manage tor service
#
class site_tor (
  Array  $packages = [
    'tor',
    'tor-geoipdb',
    'torsocks',
  ],
  String $serviceconfig = '',
) {
  package { $packages:
    ensure => installed,
  }

  -> file { '/etc/tor/torrc':
    ensure  => present,
    mode    => '0640',
    owner   => 'root',
    group   => 'root',
    content => template('site_tor/torrc.erb'),
  }

  ~> service { 'tor@default':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }
}
