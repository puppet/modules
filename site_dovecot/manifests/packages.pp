# Class to install dovecot related packages
#
class site_dovecot::packages(
  Array $packages = [
  'dovecot-core',
  'dovecot-imapd',
  'dovecot-ldap',
  'dovecot-pop3d',
  'sasl2-bin',
  ],
){
  package { $packages :
    ensure => installed,
  }
}
