# Main class for custom dovecot config
#
class site_dovecot {
  include ::profile::domaincerts
  include ::site_dovecot::packages
  include ::site_dovecot::config

  service { 'dovecot':
    ensure     => running,
    hasstatus  => true,
    hasrestart => true,
  }

  service { 'saslauthd':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }

  Class['::site_dovecot::packages']
  -> Class['::site_dovecot::config']
  ~> Service['dovecot']

  Class['::site_dovecot::packages']
  -> Class['::site_dovecot::config']
  ~> Service['saslauthd']

  Class['::profile::domaincerts'] ~> Service['dovecot']
}
