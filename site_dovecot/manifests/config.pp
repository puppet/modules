# Class to manage configuration files for dovecot
#
class site_dovecot::config (
  String $prefix   = '/etc/dovecot',
  String $certfile = $::profile::domaincerts::bundlefile,
  String $keyfile  = $::profile::domaincerts::keyfile
) {
  file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0640';
    "${prefix}/conf.d/10-logging.conf":
      source => 'puppet:///modules/site_dovecot/logging.conf';
    "${prefix}/conf.d/10-master.conf":
      source => 'puppet:///modules/site_dovecot/master.conf';
    "${prefix}/conf.d/10-mail.conf":
      source => 'puppet:///modules/site_dovecot/mail.conf';
    "${prefix}/conf.d/10-ssl.conf":
      content => template('site_dovecot/ssl.conf.erb');
    '/etc/default/saslauthd':
      source => 'puppet:///modules/site_dovecot/saslauthd';
  }

  user { 'dovecot':
    ensure => present,
    groups => [ 'ssl-cert' ],
  }
}
